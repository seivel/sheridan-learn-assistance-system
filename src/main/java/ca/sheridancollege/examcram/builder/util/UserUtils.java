package ca.sheridancollege.examcram.builder.util;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

public final class UserUtils {

    private static final int VISIBILITY = 2;
    private static final String ENCRYPT_TEXT = "*****";
    private static final int DIGITS_OF_VERIFY_CODE = 6;

    private UserUtils() {
    }

    /**
     * Build response map with encrypted email and ticket
     *
     * @param email  email
     * @param ticket ticket
     * @return map with encrypted email and ticket
     */
    public static Map<String, String> buildResponse(String email, String ticket) {
        return Map.of("email", encryptEmail(email), "ticket", ticket);
    }

    /**
     * Used to encrypt email address
     *
     * @param email email
     * @return encrypted email
     */
    private static String encryptEmail(String email) {
        final StringBuilder sb = new StringBuilder();

        final int index = email.indexOf('@');
        final char[] buffer = email.toCharArray();

        return sb.append(buffer, 0, VISIBILITY)
                .append(ENCRYPT_TEXT)
                .append(buffer, index, buffer.length - index)
                .toString();
    }

    /**
     * Generate random ticket
     *
     * @return 32-bit ticket
     */
    public static String generateTicket() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * Generate verification code
     *
     * @return verification code
     */
    public static String generateVerifyCode() {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < DIGITS_OF_VERIFY_CODE; i++) {
            sb.append(Character.toChars(random.nextInt('Z' - 'A') + 'A'));
        }

        return sb.toString();
    }

}
