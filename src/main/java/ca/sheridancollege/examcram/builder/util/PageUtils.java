package ca.sheridancollege.examcram.builder.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Optional;

public final class PageUtils {

    private final static String[] SORT_KEYWORD = new String[]{"asc", "desc"};

    private PageUtils() {
    }

    /**
     * Build pageable
     *
     * @param page page
     * @param size size
     * @param sort sort option, either asc or desc
     * @return pageable
     */
    public static Pageable buildPageable(Optional<Integer> page,
                                         Optional<Integer> size,
                                         Optional<String> sort) {
        final String[] split = sort.orElse("").split(",");

        Optional<Sort> sortParam = Optional.empty();

        if (split[0].length() > 0) {
            final String[] properties = Arrays.stream(split)
                    .filter(s -> !Arrays.asList(SORT_KEYWORD).contains(s)).toArray(String[]::new);

            sortParam = Optional.of(Sort.by(
                    Arrays.asList(split).contains(SORT_KEYWORD[1]) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    properties));
        }

        return PageRequest.of(
                page.orElse(0),
                size.orElse(20),
                sortParam.orElse(Sort.unsorted()));
    }
}
