package ca.sheridancollege.examcram.builder.util;

import ca.sheridancollege.examcram.builder.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Objects;

public final class FileUtils {

    private final static String BASE_PATH = "./uploads";
    private final static String CONTENT_TYPE_START = "image";

    private FileUtils() {
    }

    /**
     * Check if file is image
     *
     * @param file multipart file
     * @throws CustomException when file is not a image
     */
    public static void checkImage(MultipartFile file) throws CustomException {
        if (!Objects.requireNonNull(file.getContentType()).startsWith(CONTENT_TYPE_START)) {
            throw new CustomException("Image Only", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Used to get hashed filename
     *
     * @param file original file
     * @return hashed file name with extension
     * @throws IOException if failed when get bytes for file
     */
    public static String getFilename(MultipartFile file) throws IOException {
        return DigestUtils.md5DigestAsHex(file.getBytes()) +
                file.getOriginalFilename()
                        .substring(Objects.requireNonNull(file.getOriginalFilename()).indexOf('.'))
                        .toLowerCase(Locale.ENGLISH);
    }

    /**
     * Used to get the path for a file
     *
     * @param filename filename
     * @return the path for the file
     * @throws IOException if failed when create parent directories
     */
    public static Path resolvePath(String filename) throws IOException {
        final Path path = Paths.get(BASE_PATH)
                .toAbsolutePath()
                .normalize()
                .resolve(filename.substring(0, 2))
                .resolve(filename.substring(2, 4));

        Files.createDirectories(path);

        return path.resolve(filename);
    }

    /**
     * Used to remove file
     *
     * @param filename filename
     * @throws IOException if failed to resolve path
     */
    public static boolean removeFile(String filename) throws IOException {
        return resolvePath(filename).toFile().delete();
    }
}
