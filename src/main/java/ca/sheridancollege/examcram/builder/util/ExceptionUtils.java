package ca.sheridancollege.examcram.builder.util;

import ca.sheridancollege.examcram.builder.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Optional;

public final class ExceptionUtils {

    private ExceptionUtils() {
    }

    /**
     * Throw new not found exception
     *
     * @throws CustomException with Not Found message and 404 status code
     */
    public static <T> T get(Optional<T> optional) throws CustomException {
        return optional.orElseThrow(() -> new CustomException("Not Found", HttpStatus.NOT_FOUND));
    }

    /**
     * Check if time is before now
     *
     * @param time local date time
     * @throws CustomException when time is after current time, with Expired message and 400 status code
     */
    public static void isExpired(LocalDateTime time) throws CustomException {
        if (LocalDateTime.now().isAfter(time)) {
            throw new CustomException("Expired", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Compare two strings
     *
     * @param str1 string 1
     * @param str2 string 2
     * @throws CustomException when two stings not identical, with Not Verified message and 422 status code
     */
    public static void isEqual(String str1, String str2) throws CustomException {
        if (StringUtils.isEmpty(str1) || !str1.equalsIgnoreCase(str2)) {
            throw new CustomException("Not Verified", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
