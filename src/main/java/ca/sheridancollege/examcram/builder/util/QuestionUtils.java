package ca.sheridancollege.examcram.builder.util;

import ca.sheridancollege.examcram.builder.entity.projection.QuestionWithIdAndDifficulty;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public final class QuestionUtils {

    private static final int[] BOUNDARY = new int[]{0, 40, 50, 60, 100};

    private QuestionUtils() {
    }

    /**
     * Generate a sorted integer array that has certain number of integers
     * that has the desired sum.
     *
     * @param num number of integers
     * @param sum sum of integers
     * @return generated integer array
     */
    public static int[] getRandomIntArray(int num, int sum) {
        final Random rand = new Random();

        final int[] v = rand.ints(num, 1, sum).sorted().toArray();

        int[] w = new int[num];

        for (int i = 0; i < num - 1; i++) {
            w[i] = v[i + 1] - v[i];
        }

        w[num - 1] = v[0] - v[num - 1] + sum;

        return Arrays.stream(w).sorted().toArray();
    }

    /**
     * Distribute the random number to each difficulty level
     *
     * @param randArray  sorted array of random int
     * @param difficulty difficulty level
     * @return an integer array with quantity for each difficulty
     */
    public static int[] distribute(int[] randArray, int difficulty) {
        int[] quantity = new int[]{0, 0, 0};

        int i = 0;

        if (difficulty >= BOUNDARY[i] && difficulty < BOUNDARY[++i]) {
            // First range: 0-40, Easy > Med > Hard
            quantity[0] = randArray[0];
            quantity[1] = randArray[1];
            quantity[2] = randArray[2];
        } else if (difficulty >= BOUNDARY[i] && difficulty < BOUNDARY[++i]) {
            // Second range: 40-50, Med > Easy > Hard
            quantity[0] = randArray[1];
            quantity[1] = randArray[0];
            quantity[2] = randArray[2];
        } else if (difficulty >= BOUNDARY[i] && difficulty < BOUNDARY[++i]) {
            // Third range: 50-60, Med > Hard > Easy
            quantity[0] = randArray[2];
            quantity[1] = randArray[0];
            quantity[2] = randArray[1];
        } else if (difficulty >= BOUNDARY[i] && difficulty < BOUNDARY[++i]) {
            // Forth range, 60-100, Hard > Med > Easy
            quantity[0] = randArray[2];
            quantity[1] = randArray[1];
            quantity[2] = randArray[0];
        }

        return quantity;
    }

    /**
     * Remove item include first list that also appeared include second list
     *
     * @param include first list
     * @param exclude second list
     * @return removed list
     */
    public static List<QuestionWithIdAndDifficulty> removeRepeated(List<QuestionWithIdAndDifficulty> include,
                                                                   List<BigInteger> exclude) {
        final List<QuestionWithIdAndDifficulty> remove = new CopyOnWriteArrayList<>();

        include.forEach(i -> {
            if (exclude.contains(BigInteger.valueOf(i.getId()))) {
                remove.add(i);
            }
        });

        include.removeAll(remove);

        return include;
    }

    /**
     * Random select items in the list
     *
     * @param quantity number of items to be selected
     * @param qq       Candidate list
     * @return random selected list
     */
    public static List<Long> selectRandom(long quantity, List<Long> qq) {
        final Random rand = new Random();

        final List<Long> ids = new CopyOnWriteArrayList<>();

        if (qq.size() < quantity) {
            // Number of question in this difficulty is less than expected
            ids.addAll(qq);
        } else {
            final List<Integer> index = new CopyOnWriteArrayList<>();

            // Generate a series of random indexes
            for (int i = 0; i < quantity; i++) {
                int temp;

                do {
                    temp = rand.nextInt(qq.size());
                } while (index.contains(temp));

                index.add(temp);
            }

            // Collect id for selected questions
            index.forEach(i -> ids.add(qq.get(i)));
        }

        return ids;
    }

}
