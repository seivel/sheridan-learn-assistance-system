package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@RepositoryRestController
@RequestMapping(path = "/programs")
public class ProgramController {

    private final ProgramService programService;

    @Autowired
    public ProgramController(ProgramService programService) {
        this.programService = programService;
    }

    @GetMapping(path = "/full")
    public ResponseEntity<?> listProgramFull() {

        return ResponseEntity.ok(Map.of("content", programService.listProgramFull()));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteProgramById(@PathVariable long id) {

        programService.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
