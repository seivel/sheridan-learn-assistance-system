package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.service.PasswordResetService;
import ca.sheridancollege.examcram.builder.service.RegisterService;
import ca.sheridancollege.examcram.builder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RepositoryRestController
@RequestMapping(path = "/users")
public class UserController {

    private final UserService userService;
    private final RegisterService registerService;
    private final PasswordResetService passwordResetService;

    @Autowired
    public UserController(UserService userService,
                          RegisterService registerRequest,
                          PasswordResetService passwordResetService) {
        this.userService = userService;
        this.registerService = registerRequest;
        this.passwordResetService = passwordResetService;
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> login(@RequestParam String username,
                                   @RequestParam String password,
                                   @RequestParam String recaptchaResponse) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    Map.of("token", userService.login(username, password, recaptchaResponse)),
                    linkTo(UserController.class,
                            UserController.class.getMethod("login", String.class, String.class, String.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).build();
        }
    }

    @PostMapping(path = "/tokenRenew")
    public ResponseEntity<?> tokenRenew(@RequestParam String token) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    Map.of("token", userService.tokenRenew(token)),
                    linkTo(UserController.class,
                            UserController.class.getMethod("tokenRenew", String.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/{username}")
    public ResponseEntity<?> update(@PathVariable String username,
                                    @RequestParam String firstName,
                                    @RequestParam String lastName) {
        try {
            userService.update(username, firstName, lastName);

            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/passwordChange/{username}")
    public ResponseEntity<?> passwordChange(@PathVariable String username,
                                            @RequestParam String oldPassword,
                                            @RequestParam String newPassword) {
        try {
            userService.passwordChange(username, oldPassword, newPassword);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/invite")
    public ResponseEntity<?> invite(@RequestParam String email) {
        try {
            registerService.invite(email);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/registerRequest")
    public ResponseEntity<?> registerRequest(@RequestParam String ticket) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    registerService.registerRequest(ticket),
                    linkTo(UserController.class,
                            UserController.class.getMethod("registerRequest", String.class)).withSelfRel()));
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(path = "/registerCodeRequest")
    public ResponseEntity<?> registerCodeRequest(@RequestParam String ticket) {
        try {
            registerService.registerCodeRequest(ticket);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/register")
    public ResponseEntity<?> register(@RequestParam String username,
                                      @RequestParam String password,
                                      @RequestParam String firstName,
                                      @RequestParam String lastName,
                                      @RequestParam String ticket,
                                      @RequestParam String vCode) {
        final User user = new User(username, password, firstName, lastName);

        try {
            registerService.register(user, ticket, vCode);

            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/passwordResetRequest")
    public ResponseEntity<?> passwordResetRequest(@RequestParam String email) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    passwordResetService.passwordResetRequest(email),
                    linkTo(UserController.class,
                            UserController.class.getMethod("passwordResetRequest", String.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/passwordCodeRequest")
    public ResponseEntity<?> passwordCodeRequest(@RequestParam String ticket) {
        try {
            passwordResetService.passwordResetCodeRequest(ticket);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @PostMapping(path = "/passwordReset")
    public ResponseEntity<?> passwordReset(@RequestParam String ticket,
                                           @RequestParam String password,
                                           @RequestParam String vCode) {
        try {
            passwordResetService.passwordReset(password, ticket, vCode);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @Deprecated
    @PostMapping(path = "/disable/{username}")
    public ResponseEntity<?> disableUser(@PathVariable String username) {
        try {
            userService.setUserAvailability(username, false);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).build();
        }
    }

    @Deprecated
    @PostMapping(path = "/enable/{username}")
    public ResponseEntity<?> enableUser(@PathVariable String username) {
        try {
            userService.setUserAvailability(username, true);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).build();
        }
    }
}
