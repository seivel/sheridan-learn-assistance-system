package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.service.ExamCramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RepositoryRestController
@RequestMapping(path = "/examCrams")
public class ExamCramController {

    private final ExamCramService examCramService;

    @Autowired
    public ExamCramController(ExamCramService examCramService) {
        this.examCramService = examCramService;
    }

    @GetMapping(path = "/search/future")
    public ResponseEntity<?> listFuture(@RequestParam Optional<Integer> page,
                                        @RequestParam Optional<Integer> size,
                                        @RequestParam Optional<String> sort) {
        try {
            return ResponseEntity.ok(new Resources<>(
                    examCramService.listFuture(page, size, sort).getContent(),
                    linkTo(ExamCramController.class,
                            ExamCramController.class.getMethod("listFuture",
                                    Optional.class,
                                    Optional.class,
                                    Optional.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @GetMapping(path = "/search/present")
    public ResponseEntity<?> getPresent() {

        try {
            return ResponseEntity.ok(new Resources<>(
                    Collections.singletonList(examCramService.getPresent()),
                    linkTo(methodOn(ExamCramController.class).getPresent()).withSelfRel()));
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @GetMapping(path = "/search/past")
    public ResponseEntity<?> listPast(@RequestParam Optional<Integer> page,
                                      @RequestParam Optional<Integer> size,
                                      @RequestParam Optional<String> sort) {
        try {
            return ResponseEntity.ok(new Resources<>(
                    examCramService.listPast(page, size, sort).getContent(),
                    linkTo(ExamCramController.class,
                            ExamCramController.class.getMethod("listPast",
                                    Optional.class,
                                    Optional.class,
                                    Optional.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @GetMapping(path = "/stats/{id}")
    public ResponseEntity<?> getStats(@PathVariable long id) {
        return ResponseEntity.ok(new Resource<>(examCramService.getStats(id)));
    }

    @GetMapping(path = "/stats")
    public ResponseEntity<?> listStats(@RequestParam long from,
                                       @RequestParam long to) {

        return ResponseEntity.ok(examCramService.listStats(from, to));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteById(@PathVariable long id) {
        try {
            examCramService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }
}
