package ca.sheridancollege.examcram.builder.controller;

import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;

@BasePathAwareController
@RequestMapping("/ng")
public class NgController {

    @RequestMapping(path = "/**")
    public String forward() {
        return "forward:/index.html";
    }
}
