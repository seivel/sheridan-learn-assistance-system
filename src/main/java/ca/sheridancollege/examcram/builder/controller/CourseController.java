package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping(path = "/courses")
public class CourseController {


    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteCourseById(@PathVariable long id) {

        courseService.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
