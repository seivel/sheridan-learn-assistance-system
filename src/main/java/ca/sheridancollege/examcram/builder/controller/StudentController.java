package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@BasePathAwareController
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> login(@RequestParam String accessCode,
                                   @RequestParam String campus,
                                   @RequestParam String name,
                                   @RequestParam String recaptchaResponse) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    Map.of("token", studentService.login(accessCode, Campus.valueOf(campus), name, recaptchaResponse)),
                    linkTo(StudentController.class,
                            StudentController.class.getMethod("login", String.class, String.class, String.class, String.class)).withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }
}
