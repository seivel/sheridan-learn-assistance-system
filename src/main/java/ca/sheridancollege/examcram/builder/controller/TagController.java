package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping(path = "/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteTagById(@PathVariable long id) {

        tagService.deleteTagById(id);

        return ResponseEntity.noContent().build();
    }
}
