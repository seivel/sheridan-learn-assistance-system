package ca.sheridancollege.examcram.builder.controller;

import ca.sheridancollege.examcram.builder.entity.Question;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.service.CourseService;
import ca.sheridancollege.examcram.builder.service.QuestionService;
import ca.sheridancollege.examcram.builder.service.TagService;
import ca.sheridancollege.examcram.builder.util.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RepositoryRestController
@RequestMapping(path = "/questions")
public class QuestionController {

    private final QuestionService questionService;
    private final CourseService courseService;
    private final TagService tagService;

    @Autowired
    public QuestionController(QuestionService questionService,
                              CourseService courseService,
                              TagService tagService) {
        this.questionService = questionService;
        this.courseService = courseService;
        this.tagService = tagService;
    }

    @PostMapping()
    public ResponseEntity<?> saveQuestion(@RequestParam String description,
                                          @RequestParam int difficulty,
                                          @RequestParam long[] courses,
                                          @RequestParam long[] tags,
                                          @RequestParam Optional<MultipartFile> image) {
        try {
            final Question question = new Question(description,
                    difficulty,
                    tagService.getTagSet(tags),
                    courseService.getCourseSet(courses));
            return ResponseEntity.ok(new Resource<>(
                    Map.of("content", questionService.saveQuestion(question, image)),
                    linkTo(QuestionController.class, QuestionController.class
                            .getMethod("saveQuestion", String.class, int.class, long[].class, long[].class, Optional.class))
                            .withSelfRel()));
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(path = "/{id}/image")
    @PatchMapping(path = "/{id}/image")
    public ResponseEntity<?> updateImage(@PathVariable long id, @RequestParam Optional<MultipartFile> image) {

        if (image.isPresent()) {
            questionService.updateImage(id, image.get());
        } else {
            questionService.removeImage(id);
        }

        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "/image/{filename:.+}")
    public ResponseEntity<?> getImage(@PathVariable String filename, HttpServletResponse response) {
        try {
            IOUtils.copy(Files.newInputStream(FileUtils.resolvePath(filename)), response.getOutputStream());
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(path = "/studentQuery")
    public ResponseEntity<?> studentQuery(@RequestParam int difficulty,
                                          @RequestParam long courseId,
                                          @RequestParam long... tagIds) {
        try {
            return ResponseEntity.ok(new Resource<>(
                    Map.of("questions", questionService.studentQuery(difficulty, courseId, tagIds)),
                    linkTo(QuestionController.class, QuestionController.class
                            .getMethod("studentQuery", int.class, long.class, long[].class))
                            .withSelfRel()));
        } catch (NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(path = "/report")
    public ResponseEntity<?> reportQuestion(@RequestParam long id,
                                            @RequestParam boolean increase) {
        try {
            questionService.reportQuestion(id, increase);
            return ResponseEntity.ok().build();
        } catch (CustomException e) {
            return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteQuestionById(@PathVariable long id) {

        questionService.deleteQuestionById(id);

        return ResponseEntity.noContent().build();
    }
}
