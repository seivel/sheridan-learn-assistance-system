package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.entity.ExamCramStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ExamCramStatsRepository extends JpaRepository<ExamCramStats, Long> {

    /**
     * Get all exam cram stats
     *
     * @param pageable paging parameters
     * @return Page<ExamCramStats>
     */
    @Override
    Page<ExamCramStats> findAll(Pageable pageable);

    /**
     * Find exam cram stats by id
     *
     * @param aLong exam cram stats id
     * @return Optional<ExamCramStats>
     */
    @Override
    Optional<ExamCramStats> findById(Long aLong);

    /**
     * Save or update exam cram stats
     *
     * @param s exam cram stats
     * @return saved exam cram stats
     */
    @Override
    <S extends ExamCramStats> S save(S s);

    /**
     * Delete exam cram stats by id
     *
     * @param aLong exam cram stats id
     */
    @Override
    void deleteById(Long aLong);

    /**
     * Find exam cram stats by exam cram id and campus name
     *
     * @param examCramId exam cram id
     * @param campus     campus name
     * @return Optional<ExamCramStats>
     */
    Optional<ExamCramStats> findTopByExamCramIdAndCampus(long examCramId, Campus campus);

    /**
     * Record number of student for a exam cram
     *
     * @param id     exam cram id
     * @param campus campus name
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "ExamCramStats")
    @Query(value = "UPDATE exam_cram_stats e " +
            "SET e.num_of_student = e.num_of_student + 1 " +
            "WHERE e.exam_cram_id = :id AND e.campus = :campus", nativeQuery = true)
    void recordNumOfStudent(@Param("id") long id, @Param("campus") String campus);
}
