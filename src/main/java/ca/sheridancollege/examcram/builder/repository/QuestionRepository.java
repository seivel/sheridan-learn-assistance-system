package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Question;
import ca.sheridancollege.examcram.builder.entity.projection.QuestionWithCourseAndTag;
import ca.sheridancollege.examcram.builder.entity.projection.QuestionWithIdAndDifficulty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(excerptProjection = QuestionWithCourseAndTag.class)
public interface QuestionRepository extends JpaRepository<Question, Long> {

    /**
     * Get all questions
     *
     * @param pageable paging parameters
     * @return Page<Question>
     */
    @Override
    Page<Question> findAll(Pageable pageable);

    /**
     * Get question by id
     *
     * @param aLong question id
     * @return Optional<Question>
     */
    @Override
    Optional<Question> findById(Long aLong);

    /**
     * Save or update question
     *
     * @param s Question
     * @return Question
     */
    @Override
    <S extends Question> S save(S s);

    /**
     * Delete question by id
     *
     * @param aLong question id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    /**
     * Find question under a course and contains one of the tags
     *
     * @param available question availability
     * @param courseId  course id
     * @param tagIds    array of tag ids
     * @return list of qualified questions that only contains id and difficulty
     */
    List<QuestionWithIdAndDifficulty> findDistinctByAvailableAndCoursesIdAndTagsIdIn(boolean available,
                                                                                     long courseId,
                                                                                     long... tagIds);

    /**
     * Find question under a course that contains the tags other than listed tags
     *
     * @param courseId course id
     * @param tagIds   array if tag ids
     * @return list of qualified questions that only has id field
     */
    @Query(value = "SELECT DISTINCT q.question_id FROM question q " +
            "INNER JOIN question_course c ON c.question_id = q.question_id " +
            "INNER JOIN question_tag t ON t.question_id = q.question_id " +
            "WHERE q.is_available = TRUE " +
            "AND c.course_id = :courseId AND t.tag_id NOT IN :tagIds", nativeQuery = true)
    List<BigInteger> findTagsIdNotIn(@Param("courseId") long courseId,
                                     @Param("tagIds") long... tagIds);

    /**
     * Add complaint to question
     *
     * @param id question id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Question")
    @Query(value = "UPDATE question " +
            "SET num_of_complaint = num_of_complaint + 1 " +
            "WHERE question_id = :id", nativeQuery = true)
    void addComplain(@Param("id") long id);

    /**
     * Remove complaint to question
     *
     * @param id question id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Question")
    @Query(value = "UPDATE question " +
            "SET num_of_complaint = num_of_complaint - 1 " +
            "WHERE question_id = :id", nativeQuery = true)
    void removeComplain(@Param("id") long id);

    /**
     * Delete question tag relationship by question id
     *
     * @param id question id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Question")
    @Query(value = "DELETE FROM question_tag " +
            "WHERE question_id = :id", nativeQuery = true)
    void removeQuestionTagById(@Param("id") long id);

    /**
     * Delete question tag relationship by question id
     *
     * @param id question id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Question")
    @Query(value = "DELETE FROM question_course " +
            "WHERE question_id = :id", nativeQuery = true)
    void removeQuestionCourseById(@Param("id") long id);

}
