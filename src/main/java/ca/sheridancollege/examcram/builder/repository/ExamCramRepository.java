package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.ExamCram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ExamCramRepository extends JpaRepository<ExamCram, Long> {

    /**
     * Get all exam crams
     *
     * @param pageable paging parameters
     * @return Page<ExamCram>
     */
    @Override
    Page<ExamCram> findAll(Pageable pageable);

    /**
     * Get exam cram by id
     *
     * @param aLong exam cram id
     * @return Optional<ExamCram>
     */
    @Override
    Optional<ExamCram> findById(Long aLong);

    /**
     * Save or update exam cram
     *
     * @param s ExamCram
     * @return ExamCram
     */
    @Override
    <S extends ExamCram> S save(S s);

    /**
     * Delete exam cram by id
     *
     * @param aLong exam cram id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);


    /**
     * Get latest exam cram which start before first time and end after second time
     * Used to get presently opened exam cram when both time are current time
     *
     * @param time1 LocalDateTime
     * @param time2 LocalDateTime
     * @return Optional<ExamCram>
     */
    @RestResource(exported = false)
    Optional<ExamCram> findTopByStartTimeBeforeAndEndTimeAfterOrderByEndTime(LocalDateTime time1,
                                                                             LocalDateTime time2);

    /**
     * Get list of exam crams within the specific time range
     *
     * @param from from date time
     * @param to   to date time
     * @return list of exam crams within the specific time range
     */
    List<ExamCram> findAllByStartTimeAfterAndEndTimeBeforeOrderByStartTime(LocalDateTime from,
                                                                           LocalDateTime to);

    /**
     * Used to find future exam crams
     *
     * @param time     LocalDateTime
     * @param pageable paging parameter
     * @return Page<ExamCram>
     */
    @RestResource(exported = false)
    Page<ExamCram> findAllByStartTimeAfterOrderByStartTime(LocalDateTime time, Pageable pageable);

    /**
     * Used to find past exam crams
     *
     * @param time     LocalDateTime
     * @param pageable paging parameter
     * @return Page<ExamCram>
     */
    @RestResource(exported = false)
    Page<ExamCram> findAllByEndTimeBeforeOrderByEndTime(LocalDateTime time, Pageable pageable);
}
