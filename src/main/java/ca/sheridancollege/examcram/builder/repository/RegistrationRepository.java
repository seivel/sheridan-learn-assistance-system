package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {

    /**
     * Find by registration ticket
     *
     * @param ticket registration ticket
     * @return Optional<Registration>
     */
    @RestResource(exported = false)
    Optional<Registration> findByTicket(String ticket);
}
