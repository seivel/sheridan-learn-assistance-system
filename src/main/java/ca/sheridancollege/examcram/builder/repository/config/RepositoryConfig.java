package ca.sheridancollege.examcram.builder.repository.config;

import ca.sheridancollege.examcram.builder.entity.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Program.class);
        config.exposeIdsFor(Course.class);
        config.exposeIdsFor(Tag.class);
        config.exposeIdsFor(Question.class);
        config.exposeIdsFor(ExamCram.class);
    }
}
