package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    /**
     * Get all courses
     *
     * @param pageable paging parameters
     * @return Page<Course>
     */
    @Override
    Page<Course> findAll(Pageable pageable);

    /**
     * Get course by id
     *
     * @param aLong course id
     * @return Optional<Course>
     */
    @Override
    Optional<Course> findById(Long aLong);

    /**
     * Save or update course
     *
     * @param s course
     * @return saved course
     */
    @Override
    <S extends Course> S save(S s);

    /**
     * Delete course by id
     *
     * @param aLong course id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    /**
     * Delete question course relationship by course id
     *
     * @param id course id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Tag")
    @Query(value = "DELETE FROM question_course " +
            "WHERE course_id = :id", nativeQuery = true)
    void removeQuestionCourseById(@Param("id") long id);
}
