package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {

    /**
     * Get all tags
     *
     * @param pageable paging parameters
     * @return Page<Tag>
     */
    @Override
    Page<Tag> findAll(Pageable pageable);

    /**
     * Get tag by id
     *
     * @param aLong tag id
     * @return Optional<Tag>
     */
    @Override
    Optional<Tag> findById(Long aLong);

    /**
     * Save or update tag
     *
     * @param s Tag
     * @return Tag
     */
    @Override
    <S extends Tag> S save(S s);

    /**
     * Delete tag by id
     *
     * @param aLong tag id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    /**
     * Delete course tag relationship by tag id
     *
     * @param id tag id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Tag")
    @Query(value = "DELETE FROM course_tag " +
            "WHERE tag_id = :id", nativeQuery = true)
    void removeCourseTagById(@Param("id") long id);

    /**
     * Delete question tag relationship by tag id
     *
     * @param id tag id
     */
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackForClassName = "Tag")
    @Query(value = "DELETE FROM question_tag " +
            "WHERE tag_id = :id", nativeQuery = true)
    void removeQuestionTagById(@Param("id") long id);
}
