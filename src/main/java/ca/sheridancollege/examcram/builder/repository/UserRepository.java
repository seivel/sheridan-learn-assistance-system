package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.entity.projection.UserWithRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

@RepositoryRestResource(excerptProjection = UserWithRole.class)
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Get all users
     *
     * @param pageable paging parameter
     * @return Page<User>
     */
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Page<User> findAll(Pageable pageable);

    /**
     * Get user by id
     *
     * @param aLong user id
     * @return Optional<User>
     */
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<User> findById(Long aLong);

    /**
     * Save or update user
     *
     * @param s User
     * @return User
     */
    @Override
    <S extends User> S save(S s);

    /**
     * Delete user by id
     *
     * @param aLong user id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    /**
     * Get user by username
     *
     * @param username username
     * @return User
     */
    Optional<User> findByUsername(@Param("username") String username);

    /**
     * Find user by email
     *
     * @param email user email
     * @return Optional<User>
     */
    @RestResource(exported = false)
    Optional<User> findByEmail(String email);

    /**
     * Find user by password reset ticket
     *
     * @param ticket password reset ticket
     * @return Optional<User>
     */
    @RestResource(exported = false)
    Optional<User> findByPasswordResetTicket(String ticket);
}
