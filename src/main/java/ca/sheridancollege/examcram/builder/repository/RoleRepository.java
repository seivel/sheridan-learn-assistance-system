package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {

    /**
     * Delete role by id
     *
     * @param aLong role id
     */
    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    /**
     * Find role by role name
     *
     * @param role role name
     * @return Optional<Role>
     */
    @RestResource(exported = false)
    Optional<Role> findByRole(String role);
}
