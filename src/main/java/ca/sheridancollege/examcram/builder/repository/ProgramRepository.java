package ca.sheridancollege.examcram.builder.repository;

import ca.sheridancollege.examcram.builder.entity.Program;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

public interface ProgramRepository extends JpaRepository<Program, Long> {

    /**
     * Get all programs
     *
     * @param pageable paging parameters
     * @return Page<Program>
     */
    @Override
    Page<Program> findAll(Pageable pageable);

    /**
     * Get program by id
     *
     * @param aLong program id
     * @return Optional<Program>
     */
    @Override
    Optional<Program> findById(Long aLong);

    /**
     * Save or update program
     *
     * @param s Program
     * @return Program
     */
    @Override
    <S extends Program> S save(S s);

    /**
     * Delete program by id
     *
     * @param aLong program id
     */
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);
}
