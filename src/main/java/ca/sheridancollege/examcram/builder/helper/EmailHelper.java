package ca.sheridancollege.examcram.builder.helper;

import ca.sheridancollege.examcram.builder.exception.CustomException;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.Email;

@Component
public class EmailHelper {

    private final JavaMailSenderImpl mailSender;

    @Value("${spring.mail.username}")
    private String sendFrom;

    @Value("${user.register.address}")
    private String baseAddress;

    @Autowired
    public EmailHelper(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Send registration invitation email
     *
     * @param sendTo recipient email address
     * @param ticket registration ticket
     * @throws CustomException when error occurs while sending email, with error code 500, INTERNAL_SERVER_ERROR
     */
    public void sendRegistrationInvite(@Email String sendTo, @Length(min = 32, max = 32) String ticket) throws CustomException {
        try {
            sendEmail(sendTo,
                    "Sheridan Exam Cram Builder Registration Invitation",
                    "You are invited to join Sheridan Exam Cram Builder, "
                            + "use the following link to complete the registration process: \n"
                            + baseAddress + "/" + ticket
                            + "\nLink valid within 24 hours.");
        } catch (MessagingException e) {
            throw new CustomException("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Send registration verification code
     *
     * @param sendTo recipient email address
     * @param code   registration verification code
     * @throws CustomException when error occurs while sending email, with error code 500, INTERNAL_SERVER_ERROR
     */
    public void sendRegistrationVerify(@Email String sendTo, @Length(min = 5, max = 5) String code) throws CustomException {
        try {
            sendEmail(sendTo,
                    "Sheridan Exam Cram Builder Registration Verification Code",
                    "Your registration verify code is "
                            + code
                            + ". Code valid within 5 minutes.");
        } catch (MessagingException e) {
            throw new CustomException("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    /**
     * Send password reset verification code
     *
     * @param sendTo recipient email address
     * @param code   password reset verification code
     * @throws CustomException when error occurs while sending email, with error code 500, INTERNAL_SERVER_ERROR
     */
    public void sendPasswordResetVerify(@Email String sendTo, @Length(min = 5, max = 5) String code) throws CustomException {
        try {
            sendEmail(sendTo,
                    "Sheridan Exam Cram Builder Password Reset Verification Code",
                    "Your password reset verify code is "
                            + code
                            + ". Code valid within 5 minutes.");
        } catch (MessagingException e) {
            throw new CustomException("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void sendEmail(@Email String sendTo, String subject, String content) throws MessagingException {
        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setFrom(sendFrom);
        helper.setTo(sendTo);
        helper.setSubject(subject);
        helper.setText(content);

        mailSender.send(mimeMessage);
    }
}
