package ca.sheridancollege.examcram.builder.helper;

import ca.sheridancollege.examcram.builder.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RecaptchaHelper {

    private static final String GOOGLE_RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";

    private final RestTemplateBuilder restTemplateBuilder;

    @Value("${google.recaptcha.secret}")
    private String recaptchaSecret;

    @Autowired
    public RecaptchaHelper(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    /**
     * Used to verify recaptcha response
     *
     * @param recaptchaResponse recaptcha response
     */
    public void verifyRecaptcha(String recaptchaResponse) throws CustomException {
        Map<String, String> body = new HashMap<>(2);
        body.put("secret", recaptchaSecret);
        body.put("response", recaptchaResponse);

        ResponseEntity<Map> recaptchaResponseEntity = restTemplateBuilder.build()
                .postForEntity(GOOGLE_RECAPTCHA_VERIFY_URL + "?secret={secret}&response={response}",
                        body, Map.class, body);

        if (!(Boolean) recaptchaResponseEntity.getBody().get("success")) {
            throw new CustomException("Not verified", HttpStatus.BAD_REQUEST);
        }
    }
}
