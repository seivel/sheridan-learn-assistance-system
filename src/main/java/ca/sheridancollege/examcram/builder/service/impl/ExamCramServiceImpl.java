package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.entity.ExamCram;
import ca.sheridancollege.examcram.builder.entity.ExamCramStats;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.ExamCramRepository;
import ca.sheridancollege.examcram.builder.repository.ExamCramStatsRepository;
import ca.sheridancollege.examcram.builder.service.ExamCramService;
import ca.sheridancollege.examcram.builder.util.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class ExamCramServiceImpl implements ExamCramService {

    private final ExamCramRepository examCramRepository;
    private final ExamCramStatsRepository examCramStatsRepository;


    @Autowired
    public ExamCramServiceImpl(ExamCramRepository examCramRepository,
                               ExamCramStatsRepository examCramStatsRepository) {
        this.examCramRepository = examCramRepository;
        this.examCramStatsRepository = examCramStatsRepository;
    }

    @Override
    public void deleteById(long id) throws CustomException {
        final ExamCram ec = examCramRepository.findById(id).orElseThrow(
                () -> new CustomException("Not Found", HttpStatus.NOT_FOUND));

        if (LocalDateTime.now().isBefore(ec.getStartTime())) {
            examCramRepository.deleteById(id);
            return;
        }

        throw new CustomException("Unable to delete", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Override
    public Optional<ExamCram> getPresent() throws CustomException {
        return examCramRepository
                .findTopByStartTimeBeforeAndEndTimeAfterOrderByEndTime(LocalDateTime.now(), LocalDateTime.now());
    }

    @Override
    public Page<ExamCram> listFuture(Optional<Integer> page,
                                     Optional<Integer> size,
                                     Optional<String> sort) throws CustomException {
        return examCramRepository
                .findAllByStartTimeAfterOrderByStartTime(LocalDateTime.now(),
                        PageUtils.buildPageable(page, size, sort));
    }

    @Override
    public Page<ExamCram> listPast(Optional<Integer> page, Optional<Integer> size, Optional<String> sort) {
        return examCramRepository
                .findAllByEndTimeBeforeOrderByEndTime(LocalDateTime.now(),
                        PageUtils.buildPageable(page, size, sort));
    }

    @Override
    public Map<Campus, Integer> getStats(long id) {
        final Map<Campus, Integer> stats = new HashMap<>(Campus.values().length);

        for (Campus campus : Campus.values()) {
            stats.put(campus, examCramStatsRepository.findTopByExamCramIdAndCampus(id, campus)
                    .orElseGet(ExamCramStats::new).getNumOfStudent());
        }

        return stats;
    }

    @Override
    public List<Map<String, String>> listStats(long from, long to) {
        final List<Map<String, String>> result = new ArrayList<>();

        examCramRepository.findAllByStartTimeAfterAndEndTimeBeforeOrderByStartTime(
                LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.systemDefault()),
                LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.systemDefault()))
                .forEach(examCram -> {
                    final Map<Campus, Integer> stats = getStats(examCram.getId());
                    final Map<String, String> map = new HashMap<>(5);
                    map.put("Time", examCram.getStartTime().toString().substring(0, 10));
                    map.put(Campus.Davis.toString(), stats.get(Campus.Davis).toString());
                    map.put(Campus.HMC.toString(), stats.get(Campus.HMC).toString());
                    map.put(Campus.Trafalgar.toString(), stats.get(Campus.Trafalgar).toString());
                    map.put("Total", Integer.toString(stats.get(Campus.Davis) + stats.get(Campus.HMC) + stats.get(Campus.Trafalgar)));
                    result.add(map);
                });

        return result;
    }
}
