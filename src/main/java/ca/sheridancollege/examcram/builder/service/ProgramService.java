package ca.sheridancollege.examcram.builder.service;


import ca.sheridancollege.examcram.builder.entity.Program;

import java.util.List;

public interface ProgramService {

    /**
     * List all programs with included courses and tags
     *
     * @return list of programs
     */
    List<Program> listProgramFull();

    /**
     * Delete program by id
     *
     * @param id program id
     */
    void deleteById(long id);
}
