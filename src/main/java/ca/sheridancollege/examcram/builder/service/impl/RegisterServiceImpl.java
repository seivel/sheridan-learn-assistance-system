package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Registration;
import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.helper.EmailHelper;
import ca.sheridancollege.examcram.builder.repository.RegistrationRepository;
import ca.sheridancollege.examcram.builder.repository.RoleRepository;
import ca.sheridancollege.examcram.builder.repository.UserRepository;
import ca.sheridancollege.examcram.builder.service.RegisterService;
import ca.sheridancollege.examcram.builder.util.ExceptionUtils;
import ca.sheridancollege.examcram.builder.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Set;

@Service
public class RegisterServiceImpl implements RegisterService {

    private final RoleRepository roleRepository;
    private final RegistrationRepository registrationRepository;
    private final UserRepository userRepository;
    private final EmailHelper emailHelper;

    @Value("${user.register.ticket.expire-length}")
    private long registerTicketExpireLength;

    @Value("${user.register.code.expire-length}")
    private long registerCodeExpireLength;

    @Autowired
    public RegisterServiceImpl(RoleRepository roleRepository,
                               RegistrationRepository registrationRepository,
                               UserRepository userRepository,
                               EmailHelper emailHelper) {
        this.roleRepository = roleRepository;
        this.registrationRepository = registrationRepository;
        this.userRepository = userRepository;
        this.emailHelper = emailHelper;
    }

    @Override
    public void invite(String email) throws CustomException {
        final String ticket = UserUtils.generateTicket();

        registrationRepository.save(new Registration(email, ticket, LocalDateTime.now()));

        emailHelper.sendRegistrationInvite(email, ticket);
    }

    @Override
    public Map<String, String> registerRequest(String ticket) {
        final Registration reg = ExceptionUtils.get(registrationRepository.findByTicket(ticket));

        return UserUtils.buildResponse(reg.getEmail(), reg.getTicket());
    }

    @Override
    public void registerCodeRequest(String ticket) throws CustomException {
        final Registration reg = ExceptionUtils.get(registrationRepository.findByTicket(ticket));

        ExceptionUtils.isExpired(reg.getTicketAt().plus(registerTicketExpireLength, ChronoUnit.MILLIS));

        reg.setCode(UserUtils.generateVerifyCode());
        reg.setCodeAt(LocalDateTime.now());

        emailHelper.sendRegistrationVerify(reg.getEmail(), reg.getCode());

        registrationRepository.save(reg);
    }

    @Override
    public User register(User user, String ticket, String vCode) throws CustomException {
        final Registration reg = ExceptionUtils.get(registrationRepository.findByTicket(ticket));

        ExceptionUtils.isExpired(reg.getTicketAt().plus(registerTicketExpireLength, ChronoUnit.MILLIS));
        ExceptionUtils.isExpired(reg.getCodeAt().plus(registerTicketExpireLength, ChronoUnit.MILLIS));
        ExceptionUtils.isEqual(vCode, reg.getCode());

        user.setEmail(reg.getEmail());
        user.setRoles(Set.of(roleRepository.findByRole("ROLE_SUPV").orElseThrow(
                () -> new CustomException("Internal Error", HttpStatus.INTERNAL_SERVER_ERROR))));

        registrationRepository.deleteById(reg.getId());

        return userRepository.save(user);
    }

}
