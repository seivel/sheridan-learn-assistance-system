package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.Question;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface QuestionService {

    /**
     * Max number of questions in one question sheet
     */
    int MAX_NUMBER_OF_QUESTION = 15;

    /**
     * Used to save or update question
     *
     * @param question question metadata
     * @param image    image file
     * @return saved question
     */
    Question saveQuestion(Question question, Optional<MultipartFile> image);

    /**
     * Update question image
     *
     * @param id    question id
     * @param image new image file
     */
    void updateImage(long id, MultipartFile image);

    /**
     * Remove question image
     *
     * @param id question id
     */
    void removeImage(long id);

    /**
     * Return qualified questions based on the input
     *
     * @param difficulty difficulty level of the question list
     * @param courseId   Id of the course that questions must belongs to
     * @param tagIds     Id of the tags that questions must only contains
     * @return a list of qualified questions
     */
    List<Question> studentQuery(int difficulty, long courseId, long... tagIds);

    /**
     * Used to record student report question
     *
     * @param id       question id
     * @param increase increase number or not
     */
    void reportQuestion(long id, boolean increase);

    /**
     * Delete question by id
     *
     * @param id question id
     */
    void deleteQuestionById(long id);

}
