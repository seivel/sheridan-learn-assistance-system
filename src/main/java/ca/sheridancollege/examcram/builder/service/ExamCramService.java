package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.entity.ExamCram;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ExamCramService {

    /**
     * Custom procedures to delete exam cram by id
     * Only future exam cram can be deleted
     *
     * @param id exam cram id
     */
    void deleteById(long id);

    /**
     * Get present exam cram
     *
     * @return present exam cram
     */
    Optional<ExamCram> getPresent();

    /**
     * Used to list all future exam crams
     *
     * @param page page
     * @param size page size
     * @param sort sort parameters
     * @return list of future exam crams
     */
    Page<ExamCram> listFuture(Optional<Integer> page,
                              Optional<Integer> size,
                              Optional<String> sort);

    /**
     * Used to list all past exam crams
     *
     * @param page page
     * @param size page size
     * @param sort sort parameters
     * @return list of past exam crams
     */
    Page<ExamCram> listPast(Optional<Integer> page,
                            Optional<Integer> size,
                            Optional<String> sort);

    /**
     * Get exam cram stats by id
     *
     * @param id exam cram id
     * @return map with campus name and number of student
     */
    Map<Campus, Integer> getStats(long id);

    /**
     * List stats for all past exam crams
     *
     * @param from from date time
     * @param to until date time
     * @return list of stats of all exam crams
     */
    List<Map<String, String>> listStats(long from, long to);
}
