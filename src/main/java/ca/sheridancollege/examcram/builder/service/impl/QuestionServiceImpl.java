package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Question;
import ca.sheridancollege.examcram.builder.entity.projection.QuestionWithIdAndDifficulty;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.QuestionRepository;
import ca.sheridancollege.examcram.builder.service.QuestionService;
import ca.sheridancollege.examcram.builder.util.ExceptionUtils;
import ca.sheridancollege.examcram.builder.util.FileUtils;
import ca.sheridancollege.examcram.builder.util.QuestionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question saveQuestion(Question question, Optional<MultipartFile> oImage) {
        oImage.ifPresent(image -> question.setImageFilename(saveImage(image)));

        return questionRepository.save(question);
    }

    @Override
    public void updateImage(long id, MultipartFile image) {
        final Question question = ExceptionUtils.get(questionRepository.findById(id));

        if (question.getImageFilename() != null) {
            try {
                FileUtils.removeFile(question.getImageFilename());
            } catch (IOException e) {
                throw new CustomException("Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        question.setImageFilename(saveImage(image));
        questionRepository.save(question);
    }

    @Override
    public void removeImage(long id) {
        final Question question = ExceptionUtils.get(questionRepository.findById(id));

        try {
            FileUtils.removeFile(question.getImageFilename());
        } catch (IOException e) {
            throw new CustomException("Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        question.setImageFilename(null);

        questionRepository.save(question);
    }

    private String saveImage(MultipartFile image) {
        try {
            FileUtils.checkImage(image);

            final String filename = FileUtils.getFilename(image);
            final Path path = FileUtils.resolvePath(filename);

            Files.copy(image.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

            return filename;
        } catch (IOException e) {
            throw new CustomException("Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<Question> studentQuery(int difficulty, long courseId, long... tagIds) {

        final List<QuestionWithIdAndDifficulty> include =
                questionRepository.findDistinctByAvailableAndCoursesIdAndTagsIdIn(true, courseId, tagIds);
        final List<BigInteger> exclude =
                questionRepository.findTagsIdNotIn(courseId, tagIds);

        final List<QuestionWithIdAndDifficulty> qualified = QuestionUtils.removeRepeated(include, exclude);

        if (qualified.size() < MAX_NUMBER_OF_QUESTION) {
            // Qualified questions are less than the expected
            return buildQuestionList(
                    qualified.stream().map(QuestionWithIdAndDifficulty::getId).collect(Collectors.toList()));
        }

        return buildRandomSheet(qualified,
                QuestionUtils.distribute(QuestionUtils.getRandomIntArray(3, MAX_NUMBER_OF_QUESTION), difficulty));
    }

    @Override
    public void reportQuestion(long id, boolean increase) {
        if (increase) {
            questionRepository.addComplain(id);
        } else {
            questionRepository.removeComplain(id);
        }
    }

    @Override
    @Transactional(rollbackForClassName = "Question")
    public void deleteQuestionById(long id) {
        if (questionRepository.existsById(id)) {
            questionRepository.removeQuestionCourseById(id);
            questionRepository.removeQuestionTagById(id);
            questionRepository.deleteById(id);
        }

        throw new CustomException("Not Found", HttpStatus.NOT_FOUND);
    }

    private List<Question> buildQuestionList(List<Long> ids) {
        final List<Question> questions = new CopyOnWriteArrayList<>();

        ids.forEach(id -> questionRepository.findById(id).ifPresent(questions::add));

        questions.sort(Comparator.comparing(Question::getDifficulty));

        return questions;
    }

    private List<Question> buildRandomSheet(List<QuestionWithIdAndDifficulty> qualified, int... quantity) {

        final List<Long> ids = new CopyOnWriteArrayList<>();

        for (int i = 0; i < 3; i++) {
            final int difficulty = i + 1;

            // List of question Id for each difficulty level
            final List<Long> qq = qualified.stream().filter(q -> q.getDifficulty() == difficulty)
                    .map(QuestionWithIdAndDifficulty::getId).collect(Collectors.toList());

            // Collect random selected Ids
            ids.addAll(QuestionUtils.selectRandom(quantity[i], qq));
        }

        return buildQuestionList(ids);
    }
}
