package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Tag;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.TagRepository;
import ca.sheridancollege.examcram.builder.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Set<Tag> getTagSet(long... ids) throws CustomException {
        final Set<Tag> courses = new HashSet<>();

        Arrays.stream(ids).forEach(id ->
                courses.add(tagRepository.findById(id)
                        .orElseThrow(() -> new CustomException("Not Found", HttpStatus.NOT_FOUND))));
        return courses;
    }

    @Override
    @Transactional(rollbackForClassName = "Tag")
    public void deleteTagById(long id) {
        if (tagRepository.existsById(id)) {
            tagRepository.removeQuestionTagById(id);
            tagRepository.removeCourseTagById(id);
            tagRepository.deleteById(id);
        }

        throw new CustomException("Not Found", HttpStatus.NOT_FOUND);
    }
}
