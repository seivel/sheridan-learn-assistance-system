package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.Tag;

import java.util.Set;

public interface TagService {

    /**
     * Used to get set of tags based on the list of ids
     *
     * @param ids tag ids
     * @return set of tags
     */
    Set<Tag> getTagSet(long... ids);

    /**
     * Delete tag by id
     *
     * @param id tag id
     */
    void deleteTagById(long id);
}
