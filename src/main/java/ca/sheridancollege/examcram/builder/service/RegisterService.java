package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.User;

import java.util.Map;

public interface RegisterService {

    /**
     * Used to send invitation to new user
     *
     * @param email new user's email
     */
    void invite(String email);

    /**
     * Used to retrieve information for register ticket
     *
     * @param ticket registration ticket
     * @return encrypted email
     */
    Map<String, String> registerRequest(String ticket);

    /**
     * Used to send verification code
     *
     * @param ticket registration ticket
     */
    void registerCodeRequest(String ticket);

    /**
     * Used to register new user
     *
     * @param user   new user
     * @param ticket registration ticket
     * @param vCode  user entered verification code
     * @return saved user
     */
    User register(User user, String ticket, String vCode);

}
