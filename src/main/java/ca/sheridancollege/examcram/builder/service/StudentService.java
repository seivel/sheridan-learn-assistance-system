package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.Campus;

public interface StudentService {

    /**
     * Used to login a student
     *
     * @param accessCode        access code from student
     * @param campus            campus name
     * @param name              student name
     * @param recaptchaResponse response of recaptcha
     * @return authentication jwt token if access code matches current exam cram,
     * ortherwise return an UNAUTHORIZED status
     */
    String login(String accessCode, Campus campus, String name, String recaptchaResponse);

}
