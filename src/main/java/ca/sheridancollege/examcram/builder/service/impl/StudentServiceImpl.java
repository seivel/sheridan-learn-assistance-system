package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.entity.ExamCram;
import ca.sheridancollege.examcram.builder.entity.Role;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.helper.RecaptchaHelper;
import ca.sheridancollege.examcram.builder.repository.ExamCramStatsRepository;
import ca.sheridancollege.examcram.builder.security.JwtProvider;
import ca.sheridancollege.examcram.builder.service.ExamCramService;
import ca.sheridancollege.examcram.builder.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class StudentServiceImpl implements StudentService {

    private final ExamCramService examCramService;
    private final JwtProvider jwtProvider;
    private final RecaptchaHelper recaptchaHelper;
    private final ExamCramStatsRepository examCramStatsRepository;

    @Autowired
    public StudentServiceImpl(ExamCramService examCramService,
                              JwtProvider jwtProvider,
                              RecaptchaHelper recaptchaHelper,
                              ExamCramStatsRepository examCramStatsRepository) {
        this.examCramService = examCramService;
        this.jwtProvider = jwtProvider;
        this.recaptchaHelper = recaptchaHelper;
        this.examCramStatsRepository = examCramStatsRepository;
    }

    @Override
    public String login(String accessCode, Campus campus, String name, String recaptchaResponse) throws CustomException {
        recaptchaHelper.verifyRecaptcha(recaptchaResponse);

        final ExamCram present = examCramService.getPresent()
                .orElseThrow(() -> new CustomException("Exam Cram Not open", HttpStatus.NOT_FOUND));

        if (!accessCode.equalsIgnoreCase(present.getAccessCode())) {
            throw new CustomException("Wrong Access Code", HttpStatus.BAD_REQUEST);
        }

        examCramStatsRepository.recordNumOfStudent(present.getId(), campus.toString());

        return jwtProvider.createToken("", Collections.singleton(new Role("ROLE_STUDENT")));
    }

}
