package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.User;

public interface UserService {

    /**
     * Used to login a user
     *
     * @param username          username of the user
     * @param password          password of the user
     * @param recaptchaResponse recaptcha response
     * @return authentication jwt token if username and password both matches records,
     * otherwise return an UNAUTHORIZED status
     */
    String login(String username, String password, String recaptchaResponse);

    /**
     * Used to renew a jwt token
     *
     * @param token jwt token
     * @return a new jwt token
     */
    String tokenRenew(String token);

    /**
     * Used to update user info
     *
     * @param username  username
     * @param firstName user first name
     * @param lastName  user last name
     * @return updated user
     */
    User update(String username, String firstName, String lastName);

    /**
     * Used to change user password
     *
     * @param username    username
     * @param oldPassword old password
     * @param newPassword new password
     */
    void passwordChange(String username, String oldPassword, String newPassword);

    /**
     * Used to enable or disable an user
     *
     * @param username     username of the user who need to change the status
     * @param availability true to enable a user, or false to disable a user
     */
    void setUserAvailability(String username, boolean availability);

}
