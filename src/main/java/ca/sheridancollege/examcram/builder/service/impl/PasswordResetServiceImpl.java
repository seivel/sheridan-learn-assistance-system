package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.PasswordReset;
import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.helper.EmailHelper;
import ca.sheridancollege.examcram.builder.repository.UserRepository;
import ca.sheridancollege.examcram.builder.service.PasswordResetService;
import ca.sheridancollege.examcram.builder.util.ExceptionUtils;
import ca.sheridancollege.examcram.builder.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Service
public class PasswordResetServiceImpl implements PasswordResetService {

    private final UserRepository userRepository;
    private final EmailHelper emailHelper;

    @Value("${password.reset.ticket.expire-length}")
    private long resetTicketExpireLength;

    @Value("${password.reset.code.expire-length}")
    private long resetCodeExpireLength;

    @Autowired
    public PasswordResetServiceImpl(UserRepository userRepository,
                                    EmailHelper emailHelper) {
        this.userRepository = userRepository;
        this.emailHelper = emailHelper;
    }

    @Override
    public Map<String, String> passwordResetRequest(@Email String email) throws CustomException {
        final User user = ExceptionUtils.get(userRepository.findByEmail(email));

        user.setPasswordReset(new PasswordReset(UserUtils.generateTicket(), LocalDateTime.now()));

        userRepository.save(user);

        return UserUtils.buildResponse(email, user.getPasswordReset().getTicket());
    }

    @Override
    public void passwordResetCodeRequest(String ticket) throws CustomException {
        final User user = ExceptionUtils.get(userRepository.findByPasswordResetTicket(ticket));

        ExceptionUtils.isExpired(user.getPasswordReset().getTicketAt().plus(resetTicketExpireLength, ChronoUnit.MILLIS));

        user.getPasswordReset().setCode(UserUtils.generateVerifyCode());
        user.getPasswordReset().setCodeAt(LocalDateTime.now());

        emailHelper.sendPasswordResetVerify(user.getEmail(), user.getPasswordReset().getCode());

        userRepository.save(user);
    }

    @Override
    public void passwordReset(String password, String ticket, String vCode) throws CustomException {
        final User user = ExceptionUtils.get(userRepository.findByPasswordResetTicket(ticket));

        ExceptionUtils.isExpired(user.getPasswordReset().getTicketAt().plus(resetTicketExpireLength, ChronoUnit.MILLIS));
        ExceptionUtils.isExpired(user.getPasswordReset().getCodeAt().plus(resetCodeExpireLength, ChronoUnit.MILLIS));
        ExceptionUtils.isEqual(vCode, user.getPasswordReset().getCode());

        // check if password is resetted after the ticked is issued
        if (user.getLastResetAt() != null && user.getLastResetAt().isAfter(user.getPasswordReset().getTicketAt())) {
            // clear record
            user.setPasswordReset(null);
            throw new CustomException("Unable to Process", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        user.setPassword(password);
        user.setLastResetAt(LocalDateTime.now());
        user.setPasswordReset(null);
        userRepository.save(user);
    }

}
