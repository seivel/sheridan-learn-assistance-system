package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.exception.CustomException;

import javax.validation.constraints.Email;
import java.util.Map;

public interface PasswordResetService {

    /**
     * Used to handle user password reset request
     *
     * @param email email
     * @return response with ticket and encrypted email
     * @throws CustomException when user not found with 404 status code
     */
    Map<String, String> passwordResetRequest(@Email String email) throws CustomException;

    /**
     * Used to send verification code
     *
     * @param ticket password reset ticket
     * @throws CustomException when ticket not found with 404 status code;
     *                         when ticket expired with 400 status code;
     *                         when error occurs while sending email with 500 status code
     */
    void passwordResetCodeRequest(String ticket) throws CustomException;

    /**
     * Used to reset user password
     *
     * @param password new password
     * @param ticket   password reset ticket
     * @param vCode    user entered verification code
     * @throws CustomException with 400, 404, 422, 500 ... status code
     */
    void passwordReset(String password, String ticket, String vCode) throws CustomException;

}
