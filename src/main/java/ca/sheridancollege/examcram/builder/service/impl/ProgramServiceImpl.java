package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Program;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.ProgramRepository;
import ca.sheridancollege.examcram.builder.service.CourseService;
import ca.sheridancollege.examcram.builder.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramServiceImpl implements ProgramService {

    private final ProgramRepository programRepository;
    private final CourseService courseService;

    @Autowired
    public ProgramServiceImpl(ProgramRepository programRepository, CourseService courseService) {
        this.programRepository = programRepository;
        this.courseService = courseService;
    }

    @Override
    public List<Program> listProgramFull() {
        return programRepository.findAll();
    }

    @Override
    public void deleteById(long id) {
        if (programRepository.existsById(id)) {
            programRepository.findById(id).orElseGet(Program::new).getCourses()
                    .forEach(c -> courseService.deleteById(c.getId()));

            programRepository.deleteById(id);
        }

        throw new CustomException("Not Found", HttpStatus.NOT_FOUND);
    }
}
