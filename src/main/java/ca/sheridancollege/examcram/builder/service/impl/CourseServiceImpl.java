package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Course;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.CourseRepository;
import ca.sheridancollege.examcram.builder.service.CourseService;
import ca.sheridancollege.examcram.builder.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final TagService tagService;


    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, TagService tagService) {
        this.courseRepository = courseRepository;
        this.tagService = tagService;
    }

    @Override
    public Set<Course> getCourseSet(long... ids) {
        final Set<Course> courses = new HashSet<>();

        Arrays.stream(ids).forEach(id -> courses.add(courseRepository.findById(id)
                .orElseThrow(() -> new CustomException("Not Found", HttpStatus.NOT_FOUND))));

        return courses;
    }

    @Override
    @Transactional(rollbackForClassName = "Course")
    public void deleteById(long id) {
        if (courseRepository.existsById(id)) {
            courseRepository.findById(id).orElseGet(Course::new).getTags()
                    .forEach(t -> tagService.deleteTagById(t.getId()));

            courseRepository.removeQuestionCourseById(id);
            courseRepository.deleteById(id);
        }

        throw new CustomException("Not Found", HttpStatus.NOT_FOUND);
    }
}
