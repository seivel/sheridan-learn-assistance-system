package ca.sheridancollege.examcram.builder.service;

import ca.sheridancollege.examcram.builder.entity.Course;

import java.util.Set;

public interface CourseService {

    /**
     * Used to get set of courses based on the list of ids
     *
     * @param ids course ids
     * @return set of course
     */
    Set<Course> getCourseSet(long... ids);

    /**
     * delete course by id
     *
     * @param id course id
     */
    void deleteById(long id);
}
