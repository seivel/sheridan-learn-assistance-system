package ca.sheridancollege.examcram.builder.service.impl;

import ca.sheridancollege.examcram.builder.entity.Role;
import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.helper.RecaptchaHelper;
import ca.sheridancollege.examcram.builder.repository.UserRepository;
import ca.sheridancollege.examcram.builder.security.JwtProvider;
import ca.sheridancollege.examcram.builder.service.UserService;
import ca.sheridancollege.examcram.builder.util.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final JwtProvider jwtProvider;
    private final AuthenticationManager authenticationManager;
    private final RecaptchaHelper recaptchaHelper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           JwtProvider jwtProvider,
                           AuthenticationManager authenticationManager,
                           RecaptchaHelper recaptchaHelper) {
        this.userRepository = userRepository;
        this.jwtProvider = jwtProvider;
        this.authenticationManager = authenticationManager;
        this.recaptchaHelper = recaptchaHelper;
    }

    @Override
    public String login(String username, String password, String recaptchaResponse) throws CustomException {
        try {
            recaptchaHelper.verifyRecaptcha(recaptchaResponse);

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

            return jwtProvider.createToken(username,
                    ExceptionUtils.get(userRepository.findByUsername(username)).getRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username or password", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public String tokenRenew(String token) throws CustomException {
        if (token != null && jwtProvider.validateToken(token)) {
            final String username = jwtProvider.getUsername(token);

            if ("".equals(username)) {
                return jwtProvider.createToken("", Collections.singleton(new Role("ROLE_STUDENT")));
            } else {
                return jwtProvider.createToken(username,
                        ExceptionUtils.get(userRepository.findByUsername(username)).getRoles());
            }
        }
        throw new CustomException("Invalid token", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Override
    public User update(String username, String firstName, String lastName) throws CustomException {
        final User user = ExceptionUtils.get(userRepository.findByUsername(username));

        user.setFirstName(firstName);
        user.setLastName(lastName);

        return userRepository.save(user);
    }

    @Override
    public void passwordChange(String username, String oldPassword, String newPassword) {
        final User user = ExceptionUtils.get(userRepository.findByUsername(username));

        final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);

        if (!encoder.matches(oldPassword, user.getPassword())) {
            throw new CustomException("Wrong Password", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        user.setPassword(newPassword);
        userRepository.save(user);
    }

    @Override
    public void setUserAvailability(String username, boolean availability) throws CustomException {
        final User user = ExceptionUtils.get(userRepository.findByUsername(username));
        user.setEnabled(availability);
        userRepository.save(user);
    }

}
