package ca.sheridancollege.examcram.builder.entity.projection;

import ca.sheridancollege.examcram.builder.entity.Course;
import ca.sheridancollege.examcram.builder.entity.Program;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "CourseWithProgram", types = {Course.class})
public interface CourseWithProgram {

    /**
     * Get course name
     *
     * @return course name
     */
    String getCourse();

    /**
     * Get associated program
     *
     * @return Program
     */
    Program getProgram();
}
