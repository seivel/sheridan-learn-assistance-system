package ca.sheridancollege.examcram.builder.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "registration")
public class Registration implements Serializable {

    private static final long serialVersionUID = 2824461754352396610L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "registration_id", nullable = false, updatable = false, unique = true)
    private long id;

    @Column(name = "email")
    private String email;

    @Column(name = "register_ticket")
    private String ticket;

    @Column(name = "register_ticket_at")
    private LocalDateTime ticketAt;

    @Column(name = "register_verify_code")
    private String code;

    @Column(name = "register_verify_code_at")
    private LocalDateTime codeAt;

    public Registration(String email, String ticket, LocalDateTime ticketAt) {
        this.email = email;
        this.ticket = ticket;
        this.ticketAt = ticketAt;
    }
}
