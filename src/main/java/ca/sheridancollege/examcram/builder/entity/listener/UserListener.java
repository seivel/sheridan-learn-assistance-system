package ca.sheridancollege.examcram.builder.entity.listener;

import ca.sheridancollege.examcram.builder.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class UserListener {

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);

    @Value("${password.encode-start}")
    private String encodeStart;

    @PrePersist
    public void onCreate(User user) {
        user.setEnabled(true);
        user.setUsername(StringUtils.trimAllWhitespace(user.getUsername()));
        user.setPassword(encoder.encode(user.getPassword()));
    }

    @PreUpdate
    public void onUpdate(User user) {
        if (!user.getPassword().startsWith(encodeStart)) {
            user.setPassword(encoder.encode(user.getPassword()));
        }
    }
}
