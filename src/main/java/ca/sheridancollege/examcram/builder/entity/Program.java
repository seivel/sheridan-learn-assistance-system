package ca.sheridancollege.examcram.builder.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"courses"})
@EqualsAndHashCode(exclude = {"courses"})
@Entity
@Table(name = "program")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Program implements Serializable {

    private static final long serialVersionUID = -9104926301983397956L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "program_id", nullable = false, updatable = false, unique = true)
    private long id;

    @Column(name = "program", nullable = false)
    private String program;

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY)
    private Set<Course> courses = new HashSet<>();
}

