package ca.sheridancollege.examcram.builder.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Embeddable
public class PasswordReset implements Serializable {

    private static final long serialVersionUID = -1377114319126763717L;

    @Column(name = "reset_ticket")
    private String ticket;

    @Column(name = "reset_ticket_at")
    private LocalDateTime ticketAt;

    @Column(name = "reset_verify_code")
    private String code;

    @Column(name = "reset_verify_code_at")
    private LocalDateTime codeAt;

    public PasswordReset(String ticket, LocalDateTime ticketAt) {
        this.ticket = ticket;
        this.ticketAt = ticketAt;
    }
}
