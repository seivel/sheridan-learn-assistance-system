package ca.sheridancollege.examcram.builder.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"users"})
@EqualsAndHashCode(exclude = {"users"})
@Entity
@Table(name = "role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1317041039787454163L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "role_id", nullable = false, updatable = false, unique = true)
    private long id;

    @Column(name = "role", nullable = false, updatable = false, unique = true)
    private String role;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<User> users;

    public Role(String role) {
        this.role = role;
    }
}
