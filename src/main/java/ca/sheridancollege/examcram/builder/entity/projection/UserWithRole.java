package ca.sheridancollege.examcram.builder.entity.projection;

import ca.sheridancollege.examcram.builder.entity.Role;
import ca.sheridancollege.examcram.builder.entity.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "UserWithRole", types = {User.class})
public interface UserWithRole {

    long getId();

    String getUsername();

    String getEmail();

    String getFirstName();

    String getLastName();

    boolean isEnabled();

    Role getRoles();
}
