package ca.sheridancollege.examcram.builder.entity.listener;

import ca.sheridancollege.examcram.builder.entity.Question;

import javax.persistence.PrePersist;

public class QuestionListener {

    @PrePersist
    public void onCreate(Question question) {
        question.setAvailable(true);
    }
}
