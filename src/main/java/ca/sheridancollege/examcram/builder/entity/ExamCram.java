package ca.sheridancollege.examcram.builder.entity;

import ca.sheridancollege.examcram.builder.entity.listener.ExamCramListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"stats"})
@EqualsAndHashCode(exclude = {"stats"})
@Entity
@EntityListeners(ExamCramListener.class)
@Table(name = "exam_cram")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ExamCram implements Serializable {

    private static final long serialVersionUID = -6380206015013719464L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "exam_cram_id", nullable = false, updatable = false, unique = true)
    private long id;

    @FutureOrPresent
    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Future
    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;

    @Column(name = "access_code", nullable = false, length = 5)
    private String accessCode;

    @OneToMany(mappedBy = "examCram", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<ExamCramStats> stats = new HashSet<>();
}
