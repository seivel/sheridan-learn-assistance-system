package ca.sheridancollege.examcram.builder.entity.listener;

import ca.sheridancollege.examcram.builder.entity.Campus;
import ca.sheridancollege.examcram.builder.entity.ExamCram;
import ca.sheridancollege.examcram.builder.entity.ExamCramStats;

import javax.persistence.PrePersist;
import java.util.HashSet;
import java.util.Set;

public class ExamCramListener {

    @PrePersist
    public void onCreate(ExamCram examCram) {
        Set<ExamCramStats> stats = new HashSet<>(Campus.values().length);

        for (Campus campus : Campus.values()) {
            stats.add(new ExamCramStats(campus, examCram));
        }

        examCram.setStats(stats);
    }
}
