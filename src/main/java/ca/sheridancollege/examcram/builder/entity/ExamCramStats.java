package ca.sheridancollege.examcram.builder.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "exam_cram_stats")
public class ExamCramStats implements Serializable {

    private static final long serialVersionUID = -860770617001204827L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "stats_id", nullable = false, updatable = false, unique = true)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "campus")
    private Campus campus;

    @Column(name = "num_of_student")
    private int numOfStudent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "exam_cram_id")
    private ExamCram examCram;

    public ExamCramStats(Campus campus, ExamCram examCram) {
        this.campus = campus;
        this.examCram = examCram;
        this.numOfStudent = 0;
    }

    public ExamCramStats() {
        this.numOfStudent = 0;
    }
}
