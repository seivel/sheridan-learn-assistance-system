package ca.sheridancollege.examcram.builder.entity;

import ca.sheridancollege.examcram.builder.entity.listener.QuestionListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"tags", "courses"})
@EqualsAndHashCode(exclude = {"tags", "courses"})
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners({AuditingEntityListener.class, QuestionListener.class})
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = -8763697581586077466L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "question_id", nullable = false, updatable = false, unique = true)
    private long id;

    @Column(name = "description", columnDefinition = "text", length = 1000, nullable = false)
    private String description;

    @Min(1)
    @Max(3)
    @Column(name = "difficulty", nullable = false)
    private int difficulty;

    @Column(name = "is_available", nullable = false)
    private boolean available;

    @Min(0)
    @Column(name = "num_of_complaint", nullable = false)
    private int numOfComplaint;

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "image_filename", unique = true)
    private String imageFilename;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "question_tag",
            joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")

    )
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "question_course",
            joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    private Set<Course> courses = new HashSet<>();

    public Question(String description, @Min(1) @Max(3) int difficulty, Set<Tag> tags, Set<Course> courses) {
        this.description = description;
        this.difficulty = difficulty;
        this.tags = tags;
        this.courses = courses;
    }
}
