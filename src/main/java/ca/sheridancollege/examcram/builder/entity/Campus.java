package ca.sheridancollege.examcram.builder.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Getter
public enum Campus {
    /**
     * Campus name with campus code
     */
    Davis("DAV"),
    HMC("HMC"),
    Trafalgar("TRA");

    private String code;

    public static Campus getEnum(String str) {
        return Stream.of(Campus.values())
                .filter(c -> c.getCode().equalsIgnoreCase(str) || c.name().equalsIgnoreCase(str))
                .collect(toSingleton());
    }

    private static <T> Collector<T, ?, T> toSingleton() {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException();
                    }
                    return list.get(0);
                }
        );
    }
}
