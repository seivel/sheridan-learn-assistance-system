package ca.sheridancollege.examcram.builder.entity.projection;

public interface QuestionWithIdAndDifficulty {

    /**
     * Question Id
     *
     * @return id
     */
    long getId();

    /**
     * Question difficulty
     *
     * @return difficulty
     */
    int getDifficulty();
}
