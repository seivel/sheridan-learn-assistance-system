package ca.sheridancollege.examcram.builder.entity.projection;

import ca.sheridancollege.examcram.builder.entity.Course;
import ca.sheridancollege.examcram.builder.entity.Question;
import ca.sheridancollege.examcram.builder.entity.Tag;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "QuestionWithCourseAndTag", types = {Question.class})
public interface QuestionWithCourseAndTag {

    long getId();

    String getDescription();

    String getDifficulty();

    boolean isAvailable();

    int getNumOfComplaint();

    String getCreatedBy();

    LocalDateTime getCreatedAt();

    String getUpdatedBy();

    LocalDateTime getUpdatedAt();

    String getImageFilename();

    Tag[] getTags();

    Course[] getCourses();
}
