package ca.sheridancollege.examcram.builder.security;

import ca.sheridancollege.examcram.builder.entity.Role;
import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.isEmpty()) {
            return new org.springframework.security.core.userdetails.User(" ", " ",
                    mapRolesToAuthorities(Collections.singleton(new Role("ROLE_STUDENT"))));
        } else {
            final User user = userRepository.findByUsername(username).orElseThrow(
                    () -> new UsernameNotFoundException("Invalid username or password"));

            return new org.springframework.security.core.userdetails.User(
                    user.getUsername(),
                    user.getPassword(),
                    user.isEnabled(),
                    true,
                    true,
                    true,
                    mapRolesToAuthorities(user.getRoles()));
        }
    }

    private Set<? extends GrantedAuthority> mapRolesToAuthorities(Set<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getRole()))
                .collect(Collectors.toSet());
    }

}
