package ca.sheridancollege.examcram.builder.security;

import ca.sheridancollege.examcram.builder.entity.Role;
import ca.sheridancollege.examcram.builder.entity.User;
import ca.sheridancollege.examcram.builder.exception.CustomException;
import ca.sheridancollege.examcram.builder.repository.UserRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class JwtProvider {

    private final UserDetailsImpl userDetailsImpl;
    private final UserRepository userRepository;

    @Value("${security.jwt.secret-key}")
    private String secretKey;

    @Value("${security.jwt.expire-length}")
    private long validityInMilliseconds;

    @Value("${security.jwt.header-start}")
    private String headerStart;

    @Autowired
    public JwtProvider(UserDetailsImpl userDetailsImpl, UserRepository userRepository) {
        this.userDetailsImpl = userDetailsImpl;
        this.userRepository = userRepository;
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String username, Set<Role> roles) {

        final Claims claims = Jwts.claims().setSubject(username);
        claims.put("auth", roles.stream().map(s -> new SimpleGrantedAuthority(s.getRole()))
                .collect(Collectors.toList()));

        final Date now = new Date();
        final Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        final UserDetails userDetails = userDetailsImpl.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public String getUsername(Jws<Claims> jwt) {
        return jwt.getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest request) {
        final String bearerToken = request.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith(headerStart)) {
            return bearerToken.substring(7);
        } else {
            return null;
        }
    }

    public boolean validateToken(String token) {
        try {
            final Jws<Claims> jwt = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);

            @SuppressWarnings("unchecked") final List<SimpleGrantedAuthority> roles
                    = (List<SimpleGrantedAuthority>) jwt.getBody().get("auth");

            if (!roles.contains(new SimpleGrantedAuthority("ROLE_STUDENT"))) {
                final LocalDateTime resetTime = userRepository.findByUsername(getUsername(jwt))
                        .orElseGet(User::new).getLastResetAt();

                if (resetTime != null) {
                    return jwt.getBody()
                            .getIssuedAt()
                            .after(Date.from(resetTime.atZone(ZoneId.systemDefault()).toInstant()));
                }
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
