import { animate, keyframes, style, transition, trigger } from '@angular/animations';

export const shakeAnim = trigger('cardAnimator', [
  transition('* => shake', animate(400, keyframes([
    style({transform: 'translate3d(0, 0, 0)', offset: 0}),
    style({transform: 'translate3d(-10px, 0, 0)', offset: 0.2}),
    style({transform: 'translate3d(10px, 0, 0)', offset: 0.4}),
    style({transform: 'translate3d(-10px, 0, 0)', offset: 0.6}),
    style({transform: 'translate3d(10px, 0, 0)', offset: 0.8}),
    style({transform: 'translate3d(0, 0, 0)', offset: 1})
  ])))
]);
