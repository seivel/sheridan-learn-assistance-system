import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { HomeComponent } from '../components/home/home.component';
import { StudentComponent } from '../components/student/student.component';
import { StaffComponent } from '../components/staff/staff.component';
import { ExamcramComponent } from '../components/staff/examcram/examcram.component';
import { QuestionComponent } from '../components/staff/question/question.component';
import { TagComponent } from '../components/staff/tag/tag.component';
import { UserComponent } from '../components/staff/user/user.component';
import { AuthGuard } from '../guards/auth.guard';
import { LoginGuard } from '../guards/login.guard';
import { SelectorComponent } from '../components/student/selector/selector.component';
import { SheetComponent } from '../components/student/sheet/sheet.component';
import { StaffLoginComponent } from '../components/login/staff-login/staff-login.component';
import { StudentLoginComponent } from '../components/login/student-login/student-login.component';
import { ForgotPasswordComponent } from '../components/login/forgot-password/forgot-password.component';
import { RegisterComponent } from '../components/login/register/register.component';
import { ReportComponent } from '../components/staff/report/report.component';
import { SettingsComponent } from '../components/staff/settings/settings.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'login', component: LoginComponent,
    children: [
      {path: 'staff', component: StaffLoginComponent, canActivate: [LoginGuard]},
      {path: 'student', component: StudentLoginComponent, canActivate: [LoginGuard]},
      {path: 'forgotPassword', component: ForgotPasswordComponent, canActivate: [LoginGuard]},
      {path: 'register/:ticket', component: RegisterComponent},
      {path: '', pathMatch: 'full', component: HomeComponent}
    ]
  },
  {
    path: 'student', component: StudentComponent, canActivate: [AuthGuard],
    children: [
      {path: 'selector', component: SelectorComponent},
      {path: 'sheet', component: SheetComponent},
      {path: '', pathMatch: 'full', redirectTo: 'selector'}
    ]
  },
  {
    path: 'staff', component: StaffComponent, canActivate: [AuthGuard],
    children: [
      {path: 'examcram', component: ExamcramComponent},
      {path: 'question', component: QuestionComponent},
      {path: 'user', component: UserComponent},
      {path: 'tag', component: TagComponent},
      {path: 'report', component: ReportComponent},
      {path: 'settings', component: SettingsComponent},
      {path: 'settings/:option', component: SettingsComponent},
      {path: '', pathMatch: 'full', redirectTo: 'examcram'}
    ]
  },
  {path: 'logout', component: HomeComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
