import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const url = '/tokenRenew';

    if (this.auth.isAuthenticated() && req.url.search(url) === -1) {
      if (this.auth.isNeedRenew()) {
        this.auth.renewToken().subscribe(data => this.auth.setToken((<any> data).content.token.toString()));
      }

      const token = this.auth.getToken();

      if (token) {
        req = req.clone({
          setHeaders: {Authorization: `Bearer ${token}`}
        });
      }
    }

    return next.handle(req);
  }

}
