import { Campus } from './campus.enum';

export interface Student {
  firstName: string;
  lastName: string;
  studentNumber: string;
  accessCode: string;
  campus: Campus;
}
