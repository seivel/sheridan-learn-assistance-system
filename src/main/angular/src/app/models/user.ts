export interface User {
  username?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  enabled?: string;
  roles?: {
    role: string;
  };
}
