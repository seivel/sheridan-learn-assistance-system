import { Tag } from './tag';
import { Course } from './course';

export interface Question {
  id?: number;
  description?: string;
  difficulty?: number;
  courses?: Course[] | number[];
  tags?: Tag[] | number[];
  available?: string;
}
