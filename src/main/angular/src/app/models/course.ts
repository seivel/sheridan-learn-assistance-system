import { Program } from './program';

export interface Course {
  name?: string;
  program?: Program;
}
