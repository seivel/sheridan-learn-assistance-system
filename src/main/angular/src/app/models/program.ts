import { Course } from './course';

export interface Program {
  id?: number;
  name?: string;
  courses?: Course[] | number[];
}
