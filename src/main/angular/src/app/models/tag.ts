import { Course } from './course';

export interface Tag {
  id?: number;
  tag?: string;
  course?: Course[];
}
