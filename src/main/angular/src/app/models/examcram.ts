export interface Examcram {
  id?: number;
  startTime?: string;
  endTime?: string;
  accessCode?: string;
}
