import 'hammerjs';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './core/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './core/material.module';
import { ENgxPrintModule } from 'e-ngx-print';
import { RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { TokenInterceptor } from './core/token.interceptor';

import { AutofocusDirective } from './directives/autofocus.directive';
import { FirstUpperDirective } from './directives/first-upper.directive';
import { NumOnlyDirective } from './directives/num-only.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { LoginComponent } from './components/login/login.component';
import { StaffLoginComponent } from './components/login/staff-login/staff-login.component';
import { StudentLoginComponent } from './components/login/student-login/student-login.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { RegisterComponent } from './components/login/register/register.component';

import { StudentComponent } from './components/student/student.component';
import { SelectorComponent } from './components/student/selector/selector.component';
import { SheetComponent } from './components/student/sheet/sheet.component';

import { StaffComponent } from './components/staff/staff.component';
import { ExamcramComponent } from './components/staff/examcram/examcram.component';
import { HistorycomponentComponent } from './components/staff/examcram/historycomponent/historycomponent.component';
import { ManagecomponentComponent } from './components/staff/examcram/managecomponent/managecomponent.component';
import { QuestionComponent } from './components/staff/question/question.component';
import { AddquestionComponent } from './components/staff/question/addquestion/addquestion.component';
import { DisablequestionComponent } from './components/staff/question/disablequestion/disablequestion.component';
import { UserComponent } from './components/staff/user/user.component';
import { TagComponent } from './components/staff/tag/tag.component';

import { UpdatequestionComponent } from './components/staff/question/updatequestion/updatequestion.component';
import { UpdatesnackbarComponent } from './components/staff/question/updatequestion/updatesnackbar/updatesnackbar.component';
import { DisablesnackbarComponent } from './components/staff/question/disablequestion/disablesnackbar/disablesnackbar.component';
import { AddsnackbarComponent } from './components/staff/question/addquestion/addsnackbar/addsnackbar.component';
import { InvitecomponentComponent } from './components/staff/user/invitecomponent/invitecomponent.component';
import { EnablequestionComponent } from './components/staff/question/enablequestion/enablequestion.component';
import { EnablesnackbarComponent } from './components/staff/question/enablequestion/enablesnackbar/enablesnackbar.component';
import { ReportComponent } from './components/staff/report/report.component';
import { UpdatecomponentComponent } from './components/staff/user/updatecomponent/updatecomponent.component';
import { SettingsComponent } from './components/staff/settings/settings.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    StudentComponent,
    StudentLoginComponent,
    ExamcramComponent,
    HistorycomponentComponent,
    ManagecomponentComponent,
    QuestionComponent,
    UserComponent,
    StaffComponent,
    StaffLoginComponent,
    SelectorComponent,
    SheetComponent,
    AddquestionComponent,
    UpdatequestionComponent,
    DisablequestionComponent,
    AddsnackbarComponent,
    UpdatesnackbarComponent,
    DisablesnackbarComponent,
    TagComponent,
    AutofocusDirective,
    FirstUpperDirective,
    NumOnlyDirective,
    ForgotPasswordComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    InvitecomponentComponent,
    EnablequestionComponent,
    EnablesnackbarComponent,
    ReportComponent,
    UpdatecomponentComponent,
    ReportComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    ENgxPrintModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChartsModule
  ],
  entryComponents: [
    AddquestionComponent,
    DisablequestionComponent,
    UpdatequestionComponent,
    UpdatecomponentComponent,
    UpdatesnackbarComponent,
    DisablesnackbarComponent,
    AddsnackbarComponent,
    InvitecomponentComponent,
    EnablequestionComponent,
    EnablesnackbarComponent
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {siteKey: '6LfNGXoUAAAAAHUBc4Tx2GraXed0FMBdy1CdledB'} as RecaptchaSettings
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
