import { Component, OnInit } from '@angular/core';
import { shakeAnim } from '../../../core/shake.animation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../login.component.scss', './forgot-password.component.scss'],
  animations: [shakeAnim]
})
export class ForgotPasswordComponent implements OnInit {

  animationState: string;
  error: string;

  email = '';
  btnSend = true;
  btnSendText = 'Send Verify Code';
  redirectCount = 5;
  checkEmail = false;

  verifyForm: FormGroup;
  resetForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private auth: AuthService,
    private userService: UserService) { }

  ngOnInit() {
    this.verifyForm = this.fb.group({
      email: [null, Validators.compose(
        [Validators.required, Validators.email])]
    });

    this.resetForm = this.fb.group({
      password: [null, Validators.required],
      rePassword: [null, Validators.required],
      vCode: [null, Validators.required]
    }, {validator: this.matchPassword('password', 'rePassword')});
  }

  matchPassword(pass, rePass) {
    return (group: FormGroup) => {
      const password = group.controls[pass];
      const rePassword = group.controls[rePass];
      if (password.value !== rePassword.value) {
        return rePassword.setErrors({notEqual: true});
      }
    };
  }

  verify(post, stepper) {
    localStorage.removeItem('reset');
    this.userService.passwordResetRequest(post.email).subscribe(data => {
        const content = (<any>data).content;

        const reset = {
          ticket: content.ticket,
          email: content.email
        };

        localStorage.setItem('reset', JSON.stringify(reset));
        this.email = reset.email;
        stepper.next();
      },
      () => {
        this.error = 'Account not found';
        this.triggerAnimation();
      });
  }

  change(stepper) {
    this.verifyForm.reset();
    this.resetForm.reset();
    for (const i in this.verifyForm.controls) {
      this.verifyForm.controls[i].setErrors(null);
    }
    for (const i in this.resetForm.controls) {
      this.resetForm.controls[i].setErrors(null);
    }
    this.error = null;
    stepper.previous();
  }

  get() {
    const ticket = JSON.parse(localStorage.getItem('reset')).ticket;

    this.userService.passwordCodeRequest(ticket).subscribe(() => {
      this.error = null;
      this.checkEmail = true;
      this.btnSend = false;

      let counter = 60;
      const interval = setInterval(() => {
        this.btnSendText = `Send After ${counter--}s`;

        if (counter < 1) {
          clearInterval(interval);
          this.btnSendText = 'Send Verify Code';
          this.btnSend = true;
        }
      }, 1000);
    }, () => {
      this.error = 'Please go back and try again';
      this.triggerAnimation();
    });
  }

  reset(post, stepper) {
    this.error = null;
    const ticket = JSON.parse(localStorage.getItem('reset')).ticket;

    this.userService.passwordReset(ticket, post.vCode, post.password).subscribe(() => {
      stepper.next();
      localStorage.removeItem('reset');
      const interval = setInterval(() => {
        this.redirectCount--;

        if (this.redirectCount < 1) {
          this.router.navigate(['/login/staff']).catch(console.log);
          clearInterval(interval);
        }
      }, 1000);
    }, () => {
      this.error = 'Please go back and try again';
      this.triggerAnimation();
    });
  }

  triggerAnimation() {
    if (!this.animationState) {
      this.animationState = 'shake';
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }
}
