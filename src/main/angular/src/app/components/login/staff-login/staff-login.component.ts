import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { shakeAnim } from '../../../core/shake.animation';

@Component({
  selector: 'app-staff-login',
  templateUrl: './staff-login.component.html',
  styleUrls: ['../login.component.scss', 'staff-login.component.scss'],
  animations: [shakeAnim]
})
export class StaffLoginComponent implements OnInit {

  animationState: String;
  error: string;

  staffForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router) {

    this.staffForm = fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
      'recaptcha': [null, null]
    });
  }

  ngOnInit() { }

  login(captchaRef, post): void {
    this.auth.login(post.username, post.password, post.recaptcha).subscribe(
      data => {
        this.auth.setToken((<any> data).content.token.toString());

        this.router.navigate(['/staff']).catch(err => console.log(err));
      },
      () => {
        this.error = 'Incorrect Username or Password';
        this.triggerAnimation();
        captchaRef.reset();
      });
  }

  execute(captchaRef) {
    if (this.staffForm.valid) {
      captchaRef.execute();
    } else {
      this.triggerAnimation();
      captchaRef.reset();
    }
  }

  forget() {
    this.router.navigate(['/login/forgotPassword']).catch(err => console.log(err));
  }

  triggerAnimation() {
    if (!this.animationState) {
      this.animationState = 'shake';
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }
}
