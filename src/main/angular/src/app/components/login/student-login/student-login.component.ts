import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { StudentService } from '../../../services/student.service';
import { Campus } from '../../../models/campus.enum';
import { shakeAnim } from '../../../core/shake.animation';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['../login.component.scss', 'student-login.component.scss'],
  animations: [shakeAnim]
})
export class StudentLoginComponent implements OnInit {

  animationState: String;
  error: string;
  campuses: string[];

  studentForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private studentService: StudentService,
    private router: Router) {

    this.studentForm = fb.group({
      'fName': [null, Validators.required],
      'lName': [null, Validators.required],
      'stuNum': [null, Validators.compose([
        Validators.required,
        Validators.pattern(`^\\d{9}$`)])
      ],
      'code': [null, Validators.required],
      'campus': [null, Validators.required],
      'recaptcha': [null, null]
    });
  }

  ngOnInit() {
    this.campuses = Object.keys(Campus).filter(
      type => isNaN(<any>type)
    );

    this.studentForm.controls['campus'].setValue(this.campuses[0]);
  }

  checkin(captchaRef, post): void {
    this.auth.checkin(post.code, post.campus, `${post.fName} ${post.lName}`, post.recaptcha).subscribe(
      data => {
        this.auth.setToken((<any> data).content.token.toString());

        this.router.navigate(['/student']).catch(err => console.log(err));
      },
      () => {
        this.error = 'Check access code and Make sure exam cram is opening';
        this.triggerAnimation();
        captchaRef.reset();
      });
  }

  execute(captchaRef) {
    console.log(this.studentForm);
    if (this.studentForm.valid) {
      captchaRef.execute();
    } else {
      this.triggerAnimation();
      captchaRef.reset();
    }
  }

  triggerAnimation() {
    if (!this.animationState) {
      this.animationState = 'shake';
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }
}
