import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { shakeAnim } from '../../../core/shake.animation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login.component.scss', './register.component.scss'],
  animations: [shakeAnim]
})
export class RegisterComponent implements OnInit {

  animationState: string;
  error: string;
  showForm: boolean;
  success = false;

  email = '';
  btnSend = true;
  btnSendText = 'Send Verify Code';
  redirectCount = 5;
  checkEmail = false;

  ticket: string;

  registerForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private userService: UserService) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.ticket = params['ticket']);

    localStorage.removeItem('register');

    this.userService.registerRequest(this.ticket).subscribe(
      data => {
        this.showForm = true;
        const content = (<any>data).content;

        const register = {
          ticket: content.ticket,
          email: content.email
        };

        this.email = content.email;

        localStorage.setItem('register', JSON.stringify(register));
      },
      () => {
        this.showForm = false;
        this.error = `Invalid Ticket`;
      });

    this.registerForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      rePassword: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      vCode: [null, Validators.required]
    }, {validator: this.matchPassword('password', 'rePassword')});
  }

  matchPassword(pass, rePass) {
    return (group: FormGroup) => {
      const password = group.controls[pass];
      const rePassword = group.controls[rePass];
      if (password.value !== rePassword.value) {
        return rePassword.setErrors({notEqual: true});
      }
    };
  }

  get() {
    const ticket = JSON.parse(localStorage.getItem('register')).ticket;

    this.userService.registerCodeRequest(ticket).subscribe(() => {
      this.error = null;
      this.checkEmail = true;
      this.btnSend = false;

      let counter = 60;
      this.btnSendText = `Send After ${counter}s`;
      const interval = setInterval(() => {
        this.btnSendText = `Send After ${--counter}s`;

        if (counter < 1) {
          clearInterval(interval);
          this.btnSendText = 'Send Verify Code';
          this.btnSend = true;
        }
      }, 1000);
    }, () => {
      this.error = 'Please make sure link is valid';
      this.triggerAnimation();
    });
  }

  register(post) {
    const user: User = {
      username: post.username,
      firstName: post.firstName,
      lastName: post.lastName
    };

    const ticket = JSON.parse(localStorage.getItem('register')).ticket;

    this.userService.saveUser(user, post.password, ticket, post.vCode).subscribe(
      () => {
        localStorage.removeItem('register');

        this.success = true;

        const interval = setInterval(() => {
          this.redirectCount--;

          if (this.redirectCount < 1) {
            this.router.navigate(['/login/staff']).catch(console.log);
            clearInterval(interval);
          }
        }, 1000);
      },
      () => {
        this.error = 'Please make sure the link and input are valid';
        this.triggerAnimation();
      });
  }

  triggerAnimation() {
    if (!this.animationState) {
      this.animationState = 'shake';
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }
}
