import { Component, OnInit } from '@angular/core';
import { ExamCramService } from '../../services/exam-cram.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  endTime;
  startTime;
  open = false;

  logined;
  student;

  constructor(
    private auth: AuthService,
    private examCramService: ExamCramService) { }

  ngOnInit() {
    this.examCramService.getPresent().subscribe(
      data => {
        const cram = (<any> data).content[0];

        if (cram) {
          this.endTime = cram.endTime;
          this.open = true;
        } else {
          this.examCramService.listFuture().subscribe(
            data2 => {
              const cram2 = (<any> data2).content[0];

              if (cram2) {
                this.startTime = cram2.startTime;
                this.open = false;
              }
            }
          );
        }
      });

    this.logined = this.auth.isAuthenticated();

    if (this.logined) {
      this.student = this.auth.isStudent();
    }
  }
}
