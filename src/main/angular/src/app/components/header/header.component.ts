import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  student = false;
  logined = false;
  show = true;
  showMenu = false;

  name: string;

  @Input('navLinks') navLinks;

  constructor(
    private auth: AuthService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.logined = this.auth.isAuthenticated();

    if (this.logined) {
      this.student = this.auth.isStudent();

      this.userService.getByUsername(this.auth.getUsername()).subscribe(data => {
        this.name = (<any> data).firstName;
      });
    }

    this.show = !this.router.url.startsWith('/login');

    this.showMenu = this.router.url.startsWith('/staff');

  }

  logout() {
    this.auth.logout();
  }
}
