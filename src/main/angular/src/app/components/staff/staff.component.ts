import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  navLinks = [];

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.navLinks.push({path: 'examcram', label: 'Exam Cram'});
    this.navLinks.push({path: 'question', label: 'Question Management'});

    if (this.auth.isAdmin()) {
      this.navLinks.push({path: 'user', label: 'Accounts Management'});
      this.navLinks.push({path: 'tag', label: 'Tags Management'});
      this.navLinks.push({path: 'report', label: 'Report'});
    }

    this.navLinks.push({path: 'settings', label: 'Settings'});
  }

}
