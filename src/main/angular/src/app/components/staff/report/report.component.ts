import { Component, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ExamCramService } from '../../../services/exam-cram.service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  from;
  to;

  mainData;
  lineChartData;
  lineChartLabels = [];
  pieChartData = [];
  showPie = false;


  @ViewChild('lineChart') lineChart: BaseChartDirective;
  @ViewChild('pieChart') pieChart: BaseChartDirective;

  lineChartOptions: any = {
    responsive: true,
    showVerticalLine: true,
    title: {
      display: true,
      text: 'Number of Student',
      fontSize: 16
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Start Date',
          fontSize: 16
        }
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Number of Student',
          fontSize: 16
        }
      }]
    },
    tooltips: {
      mode: 'index',
      intersect: false
    },
    hover: {
      mode: 'nearest',
      intersect: false
    }
  };

  public lineChartColors: Array<any> = [
    {}, {}, {},
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

  pieChartLabels: string[] = ['Davis', 'HMC', 'Trafalgar'];

  pieChartOptions: any = {
    responsive: true,
    title: {
      display: true,
      text: '',
      fontSize: 16
    },
    legend: {
      onClick: (e) => e.stopPropagation()
    }
  };

  pieChartColors: Array<any> = [];

  constructor(private examCramService: ExamCramService) { }

  ngOnInit() {
    this.to = new Date();
    this.from = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
    this.parseData();
  }

  parseData() {
    this.mainData = [];
    this.lineChartData = [];
    this.lineChartLabels = [];
    this.examCramService.listStats(this.from.getTime(), this.to.getTime()).subscribe(
      data => this.mainData = data,
      () => console.log,
      () => {
        const dav = [];
        const hmc = [];
        const tra = [];
        const total = [];
        for (let i = 0; i < this.mainData.length; i++) {
          dav.push(this.mainData[i].Davis);
          hmc.push(this.mainData[i].HMC);
          tra.push(this.mainData[i].Trafalgar);
          total.push(this.mainData[i].Total);
          this.lineChartLabels.push(this.mainData[i].Time);
        }

        this.lineChartData = [
          {data: dav, label: 'DAV', showLine: false, pointRadius: 0, pointHitRadius: 0, pointHoverRadius: 0},
          {data: hmc, label: 'HMC', showLine: false, pointRadius: 0, pointHitRadius: 0, pointHoverRadius: 0},
          {data: tra, label: 'TRA', showLine: false, pointRadius: 0, pointHitRadius: 0, pointHoverRadius: 0},
          {data: total, label: 'Total', pointRadius: 10, pointHitRadius: 10, pointHoverRadius: 10}
        ];
      });
  }

  refresh() {
    this.showPie = false;
    this.parseData();
  }

  chartClicked(e: any): void {
    if (e.active && e.active.length > 0) {
      const index = e.active[0]._index;
      this.pieChartData = [];
      this.pieChartData.push(this.mainData[index].Davis);
      this.pieChartData.push(this.mainData[index].HMC);
      this.pieChartData.push(this.mainData[index].Trafalgar);

      this.pieChartOptions.title.text
        = `Number of Student from Each Campus on ${this.mainData[index].Time}`;

      if (this.pieChart) {
        this.pieChart.ngOnChanges({} as SimpleChanges);
      }

      this.showPie = true;
    }
  }
}
