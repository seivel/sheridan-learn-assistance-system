import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnablequestionComponent } from './enablequestion.component';

describe('EnablequestionComponent', () => {
  let component: EnablequestionComponent;
  let fixture: ComponentFixture<EnablequestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnablequestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnablequestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
