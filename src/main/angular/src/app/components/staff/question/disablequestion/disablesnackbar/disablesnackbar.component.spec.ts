import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisablesnackbarComponent } from './disablesnackbar.component';

describe('DisablesnackbarComponent', () => {
  let component: DisablesnackbarComponent;
  let fixture: ComponentFixture<DisablesnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisablesnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisablesnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
