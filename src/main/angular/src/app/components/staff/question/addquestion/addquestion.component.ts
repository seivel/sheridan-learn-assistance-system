import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramService } from '../../../../services/program.service';
import { CourseService } from '../../../../services/course.service';
import { TagService } from '../../../../services/tag.service';
import { QuestionService } from '../../../../services/question.service';
import { Question } from '../../../../models/question';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { AddsnackbarComponent } from './addsnackbar/addsnackbar.component';

@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.css']
})


export class AddquestionComponent implements OnInit {

  programFormGroup: FormGroup;
  courseFormGroup: FormGroup;
  tagFormGroup: FormGroup;

  question: Question = {};

  programs;
  courses;
  tags;
  error;

  selectedCourses = [];
  selectedTags = [];

  isTagExist = true;
  isCourseExist: boolean;

  isTagSelected = true;
  isDifficultySelected = true;
  isDescritpionEdited = true;

  imageUrl = '';
  imageFile = null;


  constructor(
    private _formBuilder: FormBuilder,
    private _programService: ProgramService,
    private _courseService: CourseService,
    private _tagService: TagService,
    private _questionService: QuestionService,
    private dialogRef: MatDialogRef<AddquestionComponent>,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this._programService.listPrograms().subscribe(data => {
      this.programs = data;
    });
    this.programFormGroup = this._formBuilder.group({
      program: [null, Validators.required]
    });

    this.courseFormGroup = this._formBuilder.group({
      course: [null, Validators.required]
    });

    this.tagFormGroup = this._formBuilder.group({
      tag: [null, Validators.required],
      difficulty: [null, Validators.required],
      description: [null, Validators.required]
    });
  }

  selectProgram(post) {
    this._courseService.listCoursesByProgram(post.program).subscribe(data => {
      this.courses = data;
      this.isCourseExist = ((this.courses.content[0].id) ? true : false);
    });

  }

  selectCourse(post) {
    if (post.course !== null) {
      this._tagService.listTagsByCourse(post.course).subscribe(data => {
        this.tags = (<any> data).content;
        this.selectedTags = [];
        this.isTagExist = ((this.tags[0].content) ? true : false);
      });
    }
  }

  tagSelectChange(e, type) {
    if (e.checked) {
      this.selectedTags.push(type.id);
    } else {
      this.selectedTags.splice(this.selectedTags.indexOf(type.id), 1);
    }

    // console.log(this.selectedTags);
  }

  add(tag) {
    const index1 = this.selectedTags.indexOf(tag);

    if (index1 < 0) {
      this.error = undefined;

      this.selectedTags.push(tag);

      const index2 = this.tags.indexOf(tag);

      if (index2 >= 0) {
        this.tags.splice(index2, 1);
      }
    }
  }

  remove(tag) {
    const index = this.selectedTags.indexOf(tag);

    if (index >= 0) {
      this.selectedTags.splice(index, 1);

  this.tags.push(tag);
}
}

  formatLabel(value: number | null) {
    if (!value) {
    return 0;
  }

    switch (value) {
      case 1: {
      return 'E';
    }
      case 2: {
      return 'N';
      }
      case 3: {
        return 'H';
      }
    }

    return value;
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.imageFile = event.target.files[0];

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        this.imageUrl = '' + reader.result;
      };
    }
  }

  removeImage() {
    this.imageUrl = '';
    this.imageFile = null;
  }

  checkBeforeInsert() {
    this.isTagSelected = this.selectedTags.length > 0;
    this.isDifficultySelected = this.tagFormGroup.controls.difficulty.value != null && this.tagFormGroup.controls.difficulty.value > 0;
    this.isDescritpionEdited = this.tagFormGroup.controls.description.value != null && this.tagFormGroup.controls.description.value != '';
  }

  insertQuestion() {
    this.checkBeforeInsert();

    if (this.isTagSelected && this.isDifficultySelected && this.isDescritpionEdited) {
      this.selectedCourses.push(this.courseFormGroup.controls.course.value);
      this.question.description = this.tagFormGroup.controls.description.value;
      this.question.difficulty = this.tagFormGroup.controls.difficulty.value;


      this._questionService.saveQuestion(this.question, this.selectedCourses, this.selectedTags.map(t => t.id), this.imageFile).subscribe(data => console.log('Add Success'));

      this.dialogRef.close();
      this.snackBar.openFromComponent(AddsnackbarComponent, {duration: 5000});
    }
  }

}
