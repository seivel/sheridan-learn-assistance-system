import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../../../services/question.service';
import { AddquestionComponent } from '../addquestion/addquestion.component';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { EnablesnackbarComponent } from './enablesnackbar/enablesnackbar.component';

@Component({
  selector: 'app-enablequestion',
  templateUrl: './enablequestion.component.html',
  styleUrls: ['./enablequestion.component.scss']
})
export class EnablequestionComponent implements OnInit {


  selectedQuestions = [];

  constructor(private questionService: QuestionService,
              private dialogRef: MatDialogRef<AddquestionComponent>,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }


  enableSelectedQuestions() {
    for (const question of this.selectedQuestions) {
      this.questionService.updateQuestionAvailability(question, true);
    }

    this.dialogRef.close();
    this.snackBar.openFromComponent(EnablesnackbarComponent, {duration: 500});
  }
}
