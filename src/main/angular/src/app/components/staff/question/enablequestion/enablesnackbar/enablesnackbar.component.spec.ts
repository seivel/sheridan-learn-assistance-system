import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnablesnackbarComponent } from './enablesnackbar.component';

describe('EnablesnackbarComponent', () => {
  let component: EnablesnackbarComponent;
  let fixture: ComponentFixture<EnablesnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnablesnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnablesnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
