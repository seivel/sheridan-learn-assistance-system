import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionService } from '../../../services/question.service';
import { MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { Question } from '../../../models/question';
import { SelectionModel } from '@angular/cdk/collections';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { DisablequestionComponent } from './disablequestion/disablequestion.component';
import { UpdatequestionComponent } from './updatequestion/updatequestion.component';
import { EnablequestionComponent } from './enablequestion/enablequestion.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  /** Columns to Display */
  displayedColumns = ['id', 'courses', 'tags', 'description', 'difficulty', 'available'];
  /**CheckBoxes*/
  selection = new SelectionModel<Question>(true, []);

  /** Data to be displayed */
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  questions;

  constructor(private questionService: QuestionService, public dialog: MatDialog, public snackBar: MatSnackBar ) { }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  listQuestions() {
    this.questionService.listQuestions().subscribe(data => {
        this.questions = data;
        this.dataSource = new MatTableDataSource<Question>(this.questions.content);

        /** Add Paginator for the Table*/
        this.dataSource.paginator = this.paginator;

        /** Add Sorting function for the Table*/
        this.dataSource.sort = this.sort;
      }
    );
  }


  /** Add Filtering function for the Table*/
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getSelectedQuestionsId(): number[] {
    const selectedItem = [];

    this.selection.selected.forEach(item => {
      // console.log(item.id);

      selectedItem.push(item.id);
    });

    return selectedItem;
  }


  openDialog() {
    const dialogRef: MatDialogRef<AddquestionComponent> = this.dialog.open(AddquestionComponent, {
      disableClose: true,
      width: '50%',
    });

    dialogRef.afterClosed().subscribe(result => {

      setTimeout(() => {
        this.listQuestions();
      }, 2000);
    });
  }

  openDisableQuestionDialog() {
    const dialogRef: MatDialogRef<DisablequestionComponent> = this.dialog.open(DisablequestionComponent, {
      disableClose: true,
      width: '50%',
    });

    dialogRef.componentInstance.selectedQuestions = this.getSelectedQuestionsId();

    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {this.listQuestions(); }, 2000);
      });
  }

  openEnableQuestionDialog() {
    const dialogRef: MatDialogRef<EnablequestionComponent> = this.dialog.open(EnablequestionComponent, {
      disableClose: true,
      width: '50%',
    });

    dialogRef.componentInstance.selectedQuestions = this.getSelectedQuestionsId();

    dialogRef.afterClosed().subscribe(result => {
      this.listQuestions();
    });

  }

  openUpdateQuestionDialog(value) {

    // const target = event.target || event.srcElement || event.currentTarget;
    // const idAttr = target.attributes.id;
    // const value = idAttr.nodeValue;

    const dialogRef: MatDialogRef<UpdatequestionComponent> = this.dialog.open(UpdatequestionComponent, {
      disableClose: true,
      width: '50%',
    });

    dialogRef.componentInstance.questionId = value;

    dialogRef.afterClosed().subscribe(result => {

      setTimeout(() => {
        this.listQuestions();
        this.snackBar.open('Question Updated!', 'Done', {duration: 5000});
      }, 2000);
    });
  }

  checkToogle(questionID: number, event) {
    const avaliability = event.checked;

    setTimeout(() => {
      this.questionService.updateQuestionAvailability(questionID, avaliability);
      avaliability ? this.snackBar.open('Question Enabled!', 'Done', {duration: 5000}) : this.snackBar.open('Question Disabled!', 'Done', {duration: 5000});
    }, 2000);

  }

  ngOnInit() {
    this.listQuestions();
  }

}
