import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramService } from '../../../../services/program.service';
import { CourseService } from '../../../../services/course.service';
import { TagService } from '../../../../services/tag.service';
import { QuestionService } from '../../../../services/question.service';
import { Question } from '../../../../models/question';
import {MatDialogRef, MatSnackBar} from "@angular/material";
import {AddquestionComponent} from "../addquestion/addquestion.component";

@Component({
  selector: 'app-updatequestion',
  templateUrl: './updatequestion.component.html',
  styleUrls: ['./updatequestion.component.scss']
})
export class UpdatequestionComponent implements OnInit {

  courseFormGroup: FormGroup;
  tagFormGroup: FormGroup;

  question: Question = {};
  currentQuestion?;
  currentCourse?;
  currentCourseId?;
  courseId;
  questionId;

  programs;
  courses;
  tags;
  error;

  isTagExist: boolean = true;

  isTagSelected = true;
  isDifficultySelected = true;
  isDescritpionEdited = true;

  selectedCourses = [];
  selectedTags = [];

  imageUrl = '';
  imageFile = null;
  imageBasePath = 'https://localhost:8443/api/v1/questions/image/';


  constructor( private _formBuilder: FormBuilder,
               private _programService: ProgramService,
               private _courseService: CourseService,
               private _tagService: TagService,
               private dialogRef: MatDialogRef<AddquestionComponent>,
               private _questionService: QuestionService,
               private snackBar: MatSnackBar) { }

  ngOnInit() {


    this._courseService.listCourses().subscribe(data =>{
      this.courses = data;
    });

    this.courseFormGroup = this._formBuilder.group({
      course: [null, Validators.required]
    });

    this.tagFormGroup = this._formBuilder.group({
      tag: [null, Validators.required],
      difficulty: [null, Validators.required],
      description: [null, Validators.required]
    });

    this.getQuestionById(this.questionId);
   // this.getCourseByQuestionId(this.questionId);
  }

  getQuestionById(questionId: number) {
    this._questionService.listQuestionById(questionId).subscribe(data => {
      this.currentQuestion = data;
      this.currentCourseId = this.currentQuestion.courses[0].id;

      if(this.currentQuestion.imageFilename !=null ){
        this.imageUrl = this.imageBasePath + this.currentQuestion.imageFilename;
      }

      this.courseFormGroup.get('course').setValue(this.currentCourseId);

      this.tagFormGroup.get('difficulty').setValue(this.currentQuestion.difficulty);
      this.tagFormGroup.get('description').setValue(this.currentQuestion.description);
    });
  }

  getCourseByQuestionId(questionId: number){
    this._questionService.listCoursesInQuestion(questionId).subscribe(data =>{
      this.currentCourse = data;
      this.currentCourseId = this.currentCourse.content[0].id;
      this.courseFormGroup.get('course').setValue(this.currentCourseId);
    });
  }

  selectProgram(post) {
    this._courseService.listCoursesByProgram(post.program).subscribe(data => {
      this.courses = data;
    });
  }



  selectCourse(post) {
    if (post.course !== null) {
      this._tagService.listTagsByCourse(post.course).subscribe(data => {
        this.tags = (<any> data).content;
        this.selectedTags = [];
        this.isTagExist = ((this.tags[0].content) ? true : false);
      });
    }
  }


  add(tag) {
    const index1 = this.selectedTags.indexOf(tag);

    if (index1 < 0) {
      this.error = undefined;

      this.selectedTags.push(tag);

      const index2 = this.tags.indexOf(tag);

      if (index2 >= 0) {
        this.tags.splice(index2, 1);
      }
    }
  }

  remove(tag) {
    const index = this.selectedTags.indexOf(tag);

    if (index >= 0) {
      this.selectedTags.splice(index, 1);

      this.tags.push(tag);
    }
  }

  formatLabel(value: number | null){
    if(!value){
      return 0;
    }

    switch(value){
      case 1:{
        return 'E';
      }
      case 2:{
        return 'N';
      }
      case 3:{
        return "H";
      }
    }

    return value;
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.imageFile = event.target.files[0];

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        this.imageUrl = '' + reader.result;
      };
    }
  }

  removeImage(){
    this.imageUrl='';
    this.imageFile = null;
  }


  checkBeforeUpdate() {
    this.isTagSelected = this.selectedTags.length > 0;
    this.isDifficultySelected = this.tagFormGroup.controls.difficulty.value != null && this.tagFormGroup.controls.difficulty.value > 0;
    this.isDescritpionEdited = this.tagFormGroup.controls.description.value != null && this.tagFormGroup.controls.description.value != '';
  }

  updateQuestion() {
    this.checkBeforeUpdate();

    console.log("This is the image file" + this.imageFile);
    if (this.isTagSelected && this.isDifficultySelected && this.isDescritpionEdited) {
      this.question.description = this.tagFormGroup.controls.description.value;
      this.question.difficulty = this.tagFormGroup.controls.difficulty.value;

      this.selectedCourses.push(this.courseFormGroup.controls.course.value);

      this._questionService.updateQuestion(this.questionId,this.question,this.selectedCourses,this.selectedTags.map(t => t.id),this.imageFile);


      this.dialogRef.close();

      setTimeout(()=>{
        this.snackBar.open("Question Updated!", "Done",{duration:5000});
      },2000);

    }
  }

}
