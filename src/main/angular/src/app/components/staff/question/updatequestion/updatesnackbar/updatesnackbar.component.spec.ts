import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatesnackbarComponent } from './updatesnackbar.component';

describe('UpdatesnackbarComponent', () => {
  let component: UpdatesnackbarComponent;
  let fixture: ComponentFixture<UpdatesnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatesnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatesnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
