import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisablequestionComponent } from './disablequestion.component';

describe('DisablequestionComponent', () => {
  let component: DisablequestionComponent;
  let fixture: ComponentFixture<DisablequestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisablequestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisablequestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
