import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../../../services/question.service';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { AddquestionComponent } from '../addquestion/addquestion.component';
import { DisablesnackbarComponent } from './disablesnackbar/disablesnackbar.component';


@Component({
  selector: 'app-disablequestion',
  templateUrl: './disablequestion.component.html',
  styleUrls: ['./disablequestion.component.css']
})
export class DisablequestionComponent implements OnInit {

  selectedQuestions = [];

  constructor(private questionService: QuestionService,
              private dialogRef: MatDialogRef<AddquestionComponent>,
              private snackBar: MatSnackBar) { }

  // disableQuestion(){
  //   this.questionService.disableQuestion(17);
  // }

  disableSelectedQuestions() {
    for (const question of this.selectedQuestions) {
      this.questionService.updateQuestionAvailability(question, false);
    }

    this.dialogRef.close();
    this.snackBar.openFromComponent(DisablesnackbarComponent, {duration: 500});
  }

  ngOnInit() {
    // console.log(this.selectedQuestions);
  }

}
