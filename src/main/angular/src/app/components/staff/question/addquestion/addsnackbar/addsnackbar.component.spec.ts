import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsnackbarComponent } from './addsnackbar.component';

describe('AddsnackbarComponent', () => {
  let component: AddsnackbarComponent;
  let fixture: ComponentFixture<AddsnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
