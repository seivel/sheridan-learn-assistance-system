import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';

@Component({
  selector: 'app-invitecomponent',
  templateUrl: './invitecomponent.component.html',
  styleUrls: ['./invitecomponent.component.scss']
})
export class InvitecomponentComponent implements OnInit {

  constructor(private userSerivce: UserService, private fb: FormBuilder, private dialogRef: MatDialogRef<InvitecomponentComponent>) { }

  verifyForm: FormGroup;

  ngOnInit() {
    this.verifyForm = this.fb.group({
      email: [null, Validators.compose(
        [Validators.required, Validators.email])]
    });
  }

  invite() {
    this.userSerivce.invite(this.verifyForm.controls.email.value).subscribe(() => console.log('success'));
    this.dialogRef.close();

  }

}
