import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';
import { MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { InvitecomponentComponent } from './invitecomponent/invitecomponent.component';
import { UpdatecomponentComponent } from './updatecomponent/updatecomponent.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  /** Columns to Display */
  displayedColumns = ['username', 'email', 'firstName', 'lastName', 'roles', 'enabled'];

  /** Data to be displayed */
  dataSource;

  users;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  listUsers(): void {
    this.userService.listUsers().subscribe(data => {
      console.log(data);
      this.users = data;
      this.dataSource = new MatTableDataSource<User>(this.users.content);

      /** Add Paginator for the Table*/
      this.dataSource.paginator = this.paginator;

      /** Add Sorting function for the Table*/
      this.dataSource.sort = this.sort;
    });
  }

  /** Add Filtering function for the Table*/
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openInviteDialog() {
    const dialogRef: MatDialogRef<InvitecomponentComponent> = this.dialog.open(InvitecomponentComponent, {
      disableClose: true,
      width: '50%',
    });
  }

  openUpdateDialog(event) {

    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;

    const dialogRef: MatDialogRef<UpdatecomponentComponent> = this.dialog.open(UpdatecomponentComponent, {
      disableClose: true,
      width: '50%',
    });

    dialogRef.componentInstance.userId = value;
  }

  checkToogle(userId: number, event) {
    const avaliability = event.checked;

    setTimeout(() => {
      this.userService.updateUserAvailability(userId, avaliability).subscribe(data => console.log(data));
      avaliability ? this.snackBar.open('User Enabled!', 'Done', {duration: 5000}) : this.snackBar.open('User Disabled!', 'Done', {duration: 5000});
    }, 2000);


  }

  ngOnInit() {
    this.listUsers();
  }

}

// export class UserDataSource extends DataSource<any> {
//   constructor( private userService: UserService) {
//     super();
//   }
//
//   connect(): Observable<User[]> {
//     return this.userService.listUsers().content;
//   }
//
//   disconnect(){}
// }
