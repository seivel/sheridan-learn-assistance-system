import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitecomponentComponent } from './invitecomponent.component';

describe('InvitecomponentComponent', () => {
  let component: InvitecomponentComponent;
  let fixture: ComponentFixture<InvitecomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitecomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitecomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
