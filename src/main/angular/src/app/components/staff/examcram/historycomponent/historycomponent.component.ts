import { Component, OnInit, ViewChild } from '@angular/core';
import { ExamCramService } from '../../../../services/exam-cram.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Examcram } from '../../../../models/examcram';

@Component({
  selector: 'app-historycomponent',
  templateUrl: './historycomponent.component.html',
  styleUrls: ['./historycomponent.component.scss']
})
export class HistorycomponentComponent implements OnInit {

  displayedColumns = ['startTime', 'endTime', 'accessCode'];

  historyExamCram;
  historyDataSource;

  @ViewChild(MatSort) historySort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  listHistory() {
    this.examCramService.listPast().subscribe(data => {
      this.historyExamCram = data;
      this.historyDataSource = new MatTableDataSource<Examcram>(this.historyExamCram.content);
      this.historyDataSource.sort = this.historySort;
      this.historyDataSource.paginator = this.paginator;
    });
  }

  constructor(private examCramService: ExamCramService) { }

  ngOnInit() {
    this.listHistory();
  }

}
