import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamcramComponent } from './examcram.component';

describe('ExamcramComponent', () => {
  let component: ExamcramComponent;
  let fixture: ComponentFixture<ExamcramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamcramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamcramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
