import { Component, OnInit, ViewChild } from '@angular/core';
import { ExamCramService } from '../../../../services/exam-cram.service';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { Examcram } from '../../../../models/examcram';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-managecomponent',
  templateUrl: './managecomponent.component.html',
  styleUrls: ['./managecomponent.component.scss']
})
export class ManagecomponentComponent implements OnInit {

  newExamForm: FormGroup;
  invalid: Boolean = false;
  errorMessage;

  presentList;
  presentExamCram: Examcram = {};

  // newStartDate;
  // newEndDate;
  // newAccessCode;

  displayedColumns = ['startTime', 'endTime', 'accessCode', 'discard'];
  futureExamCram;
  futureDataSource;

  endDate;
  startDate;
  today = new Date();

  @ViewChild(MatSort) futureSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  listFuture() {
    this.examCramService.listFuture().subscribe(data => {
      this.futureExamCram = data;
      this.futureDataSource = new MatTableDataSource<Examcram>(this.futureExamCram.content);
      this.futureDataSource.sort = this.futureSort;
      this.futureDataSource.paginator = this.paginator;
    });
  }

  getPresent() {
    this.examCramService.getPresent().subscribe(data => {
      this.presentList = data;

      this.presentExamCram = this.presentList.content[0];
      this.startDate = new Date(this.presentExamCram.startTime);
      this.endDate = new Date(this.presentExamCram.endTime);

        // console.log(this.subtractDate((new Date(this.presentExamCram.endTime)),(new Date(this.presentExamCram.startTime))));
    },
      error => {
      console.log(error);
    });
  }

  subtractDates(startDate: Date, endDate: Date) {
    return (Number(endDate) - Number(startDate));
  }

  getProgress() {

    const cramPeriod = this.subtractDates(this.startDate, this.endDate);
    const timePass = this.subtractDates(this.startDate, this.today);

    return (timePass / cramPeriod * 100);

    //  var wholePeroid:number = this.subtractDate(this.presentExamCram.endTime,this.presentExamCram.startTime);
    // var currentProgress:number = this.subtractDate(new Date(),this.presentExamCram.startTime);
    //  var result = currentProgress/wholePeroid;
  }

  // subtractDate(date1: Date, date2: Date): number {
  //   const diff = Math.abs(date1.getTime() - date2.getTime());
  //   const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
  //
  //   return diffDays;
  // }

  insertExamCram() {
    const newExamCram: Examcram = {
      startTime: this.newExamForm.controls.newStartDate.value,
      endTime: this.newExamForm.controls.newEndDate.value,
      accessCode: this.newExamForm.controls.newAccessCode.value
    };

    this.checkNewExamCram(newExamCram.startTime, newExamCram.endTime);

    if (!this.invalid) {
      this.examCramService.insertExamCram(newExamCram).subscribe(data => {
        console.log(newExamCram);
        this.newExamForm.reset();
        for (const i in this.newExamForm.controls) {
          this.newExamForm.controls[i].setErrors(null);
        }
        setTimeout(() => {
          this.listFuture();
          this.snackBar.open('New ExamCram Added Successfully!', 'Done', {duration: 5000});
        }, 2000);
      });
    }
  }

  discardExamCram(value) {

    // const target = event.target || event.srcElement || event.currentTarget;
    // const idAttr = target.attributes.id;
    // const value = idAttr.nodeValue;

    this.examCramService.discardExamCram(value).subscribe(() => {
      console.log('Discard Successfully');
      setTimeout(() => {
        this.listFuture();
        this.snackBar.open('ExamCram Removed Successfully!', 'Done', {duration: 5000});
      }, 2000);
    });


  }

  checkNewExamCram(startTime, endTime) {

    let firstInvalid = false;
    let secondInvalid = false;
    let thirdInvalid = false;

    if (startTime >= endTime) {
      firstInvalid = true;
      this.errorMessage = 'Start Date Cannot Be Larger Than End Date';
    }

    if (this.presentExamCram.id != undefined) {
      if (new Date(startTime) <= new Date(this.presentExamCram.endTime)) {
        secondInvalid = true;
        this.errorMessage = 'The Exam Cram Must Be Set For the Future';
      }
    }

    if (this.futureExamCram) {

      const loopStartDate = new Date(startTime);
      const loopEndDate = new Date(endTime);

      for (const future of this.futureExamCram.content) {
        if (loopStartDate >= new Date(future.startTime) && loopStartDate <= new Date(future.endTime)) {
          thirdInvalid = true;
          this.errorMessage = 'The Start Time Has Been Occupied';
        }

        if (loopEndDate >= new Date(future.startTime) && loopEndDate <= new Date(future.endTime)) {
          thirdInvalid = true;
          this.errorMessage = 'The End Time Has Been Occupied';
        }
      }
    }

    if (!firstInvalid && !secondInvalid && !thirdInvalid) {
      this.invalid = false;
      console.log('Success!');
    } else {
      this.invalid = true;
    }

  }

  constructor(private examCramService: ExamCramService,
              private formBuilder: FormBuilder,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.newExamForm = this.formBuilder.group({
      newStartDate: ['', Validators.required],
      newEndDate: ['', Validators.required],
      newAccessCode: ['', Validators.required]
    });
    this.listFuture();
    this.getPresent();
  }




}
