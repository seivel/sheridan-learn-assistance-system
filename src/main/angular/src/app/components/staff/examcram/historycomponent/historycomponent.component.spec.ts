import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorycomponentComponent } from './historycomponent.component';

describe('HistorycomponentComponent', () => {
  let component: HistorycomponentComponent;
  let fixture: ComponentFixture<HistorycomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorycomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorycomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
