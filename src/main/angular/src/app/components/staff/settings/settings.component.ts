import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  updateInfoForm: FormGroup;
  changePassForm: FormGroup;

  error;
  option = -1;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute) {

    this.updateInfoForm = fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required]
    });

    this.changePassForm = fb.group({
      oldPass: [null, Validators.required],
      newPass: [null, Validators.required],
      reNewPass: [null, Validators.required]
    }, {validator: this.matchPassword('newPass', 'reNewPass')});
  }

  matchPassword(pass, rePass) {
    return (group: FormGroup) => {
      const password = group.controls[pass];
      const rePassword = group.controls[rePass];
      if (password.value !== rePassword.value) {
        return rePassword.setErrors({notEqual: true});
      }
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['option']) {
        switch (params['option'].toString().toLocaleLowerCase()) {
          case 'updateinfo':
            this.option = 0;
            break;
          case 'changepassword':
            this.option = 1;
            break;
          default:
            this.option = -1;
        }
      }
    });
  }

  updateInfo(post) {
    if (this.updateInfoForm.valid) {
      this.userService.updateUser(this.auth.getUsername(), {
        firstName: post.firstName,
        lastName: post.lastName
      }).subscribe(
        () => {
          this.error = '';
          this.snackBar.open('Info Updated', 'Close');
        },
        () => {
          this.error = 'Can\'t update info. Please try again.';
        });
      this.reset(this.updateInfoForm);
    } else {
      this.error = 'Please complete the form';
    }
  }

  changePassword(post) {
    if (this.changePassForm.valid) {
      this.userService.changePassword(this.auth.getUsername(), post.oldPass, post.newPass).subscribe(
        () => {
          this.error = '';
          this.snackBar.open('Password Changed', 'Close');
        },
        () => {
          this.error = 'Old password not match the record. Can\'t change password';
        });
      this.reset(this.changePassForm);
    } else {
      this.error = 'Please complete the form';
    }
  }

  reset(form: FormGroup) {
    form.reset();
    for (const i in form.controls) {
      form.controls[i].setErrors(null);
    }
  }
}
