import { Component, OnInit } from '@angular/core';
import { ProgramService } from '../../../services/program.service';
import { TagService } from '../../../services/tag.service';
import { CourseService } from '../../../services/course.service';
import { Tag } from '../../../models/tag';
import {Program} from "../../../models/program";
import {Course} from "../../../models/course";
import { MatSnackBar } from "@angular/material";

/**
 * Node for to-do item
 */


@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
})

export class TagComponent implements OnInit{

  programs;
  courses;
  tags;

  isTagExist: boolean;
  isCourseExist: boolean;

  constructor(private programService: ProgramService,
              private courseService: CourseService,
              private tagService: TagService,
              public snackBar: MatSnackBar){}

  listPrograms(){
    this.programService.listPrograms().subscribe(data => {this.programs = data;});
  }

  selectProgram(programId: number) {
    this.courseService.listCoursesByProgram(programId).subscribe(data => {
      this.courses = data;
      this.isCourseExist = ((this.courses.content[0].id) ? true : false);
    });
  }

  selectCourse(courseId: number) {
    this.tagService.listTagsByCourse(courseId).subscribe(data => {
      this.tags = data;
      console.log(this.tags);
      this.isTagExist = ((this.tags.content[0].id) ? true : false);
    });

  }

  addTag(){
    this.courseService.deleteCourse(232).subscribe();
  }

  ngOnInit() {
    this.listPrograms();
  }

  addNewProgram(){
    let programInput = document.getElementById('newProgram')! as HTMLInputElement;
    let programValue = programInput.value;
    let newProgram: Program = {};

    newProgram.name = programValue;

    this.programService.insertProgram(newProgram).subscribe(data =>{
      programInput.value = "";
      setTimeout(()=>{
        this.snackBar.open("New Program Added, Reopen the Panel to Check Update", "Done",{duration:5000});
      },2000);
    });
  }

  updateProgram(programId: number){
    let programInput = document.getElementById('program' + programId)! as HTMLInputElement;
    let programValue = programInput.value;
    let updateProgram: Program = {};

    updateProgram.name = programValue;

    this.programService.updateProgram(programId,updateProgram).subscribe(data => {
      programInput.value = "";
      setTimeout(()=>{
        this.snackBar.open("Program Updated, Reopen the Panel to Check Update", "Done",{duration:5000});
      },2000);
    });
  }

  deleteProgram(programId: number){
    this.programService.deleteProgram(programId).subscribe();
    setTimeout(()=>{
      this.snackBar.open("Program Deleted, Reopen the Panel to Check Update", "Done",{duration:5000});
    },2000);
  }

  addNewCourse(programId: number){
    let courseInput = document.getElementById('newCourse'+programId)! as HTMLInputElement;
    let courseValue = courseInput.value;
    let newCourse:Course = {};

    newCourse.name = courseValue;

    this.courseService.insertCourse(newCourse,programId);
    courseInput.value = "";

    setTimeout(()=>{
      this.snackBar.open("New Course Added, Reopen the Panel to Check Update", "Done",{duration:5000});
    },2000);
  }

  updateCourse(courseId: number){
    let courseEdit = document.getElementById("courseEdit"+courseId)! as HTMLInputElement;
    let courseValue = courseEdit.value;
    let updatedCourse:Course = {};

    updatedCourse.name = courseValue;
    console.log('course' + courseId);
    console.log(courseId);
    console.log(courseValue);
    courseEdit.value = '';

    this.courseService.updateCourse(courseId,updatedCourse).subscribe(data =>{
      courseEdit.value = '';
      setTimeout(()=>{
        this.snackBar.open("Course Updated, Reopen the Panel to Check Update", "Done",{duration:5000});
      },2000);
    });
  }

  deleteCourse(courseId: number){
    this.courseService.deleteCourse(courseId).subscribe();
    setTimeout(()=>{
      this.snackBar.open("Course Deleted, Reopen the Panel to Check Update", "Done",{duration:5000});
    },2000);
  }

  addNewTag(courseId:number){
    let tagInput = document.getElementById('newTag'+courseId)! as HTMLInputElement;
    let courseValue = tagInput.value;
    let newTag:Tag = {};

    newTag.tag = courseValue;

    let courses = [];

    console.log(courseValue);

    courses.push(courseId);
    this.tagService.insertTag(newTag,courses);
    tagInput.value = "";

    setTimeout(()=>{
      this.snackBar.open("New Tag Added, Reopen the Panel to Check Update", "Done",{duration:5000});
    },2000);
  }

  updateTag(tagId: number){
    let tagInput = document.getElementById('updateTag' + tagId)! as HTMLInputElement;
    let tagValue = tagInput.value;
    let updatedTag: Tag = {};

    updatedTag.tag = tagValue;

    this.tagService.updateTag(tagId,updatedTag).subscribe(data => {
      setTimeout(()=>{
        this.snackBar.open("Tag Updated, Reopen the Panel to Check Update", "Done",{duration:5000});
      },2000);
    });

    tagInput.value = "";
  }

  deleteTag(tagId: number){
    this.tagService.deleteTag(tagId).subscribe();
    setTimeout(()=>{
      this.snackBar.open("Tag Deleted, Reopen the Panel to Check Update", "Done",{duration:5000});
    },2000);
  }

}


