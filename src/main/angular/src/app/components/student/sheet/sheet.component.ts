import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionService } from '../../../services/question.service';
import { Location } from '@angular/common';
import { MatCheckbox } from '@angular/material';

@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.scss']
})
export class SheetComponent implements OnInit {

  @ViewChild('print_component') printComponent;
  printTitle = 'Sheridan';
  printStyle = ['.report{display: none}'];
  imageBasePath = 'https://localhost:8443/api/v1/questions/image/';

  questions;

  constructor(
    private location: Location,
    private questionService: QuestionService) { }

  ngOnInit() {
    if (sessionStorage.getItem('questions') !== null) {
      this.questions = JSON.parse(sessionStorage.getItem('questions'));
    } else {
      this.questionService.currentQuestions.subscribe(q => {
        this.questions = (<any> q).content.questions;
        sessionStorage.setItem('questions', JSON.stringify(this.questions));
      });
    }
  }

  report(id: number, cb: MatCheckbox) {
    this.questionService.reportQuestion(id, cb.checked).subscribe();
  }

  back() {
    this.location.back();
  }

  print() {
    this.printComponent.print();
  }
}


