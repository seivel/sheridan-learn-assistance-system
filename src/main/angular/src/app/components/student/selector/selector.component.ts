import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramService } from '../../../services/program.service';
import { CourseService } from '../../../services/course.service';
import { TagService } from '../../../services/tag.service';
import { Router } from '@angular/router';
import { Tag } from '../../../models/tag';
import { MatStepper } from '@angular/material';
import { QuestionService } from '../../../services/question.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {

  programForm: FormGroup;
  courseForm: FormGroup;
  tagForm: FormGroup;

  isTagExist: boolean = true;

  programs;
  courses;
  selectedCourse;
  tags = [];
  selectedTags: Tag[] = [];
  difficulty = 50;

  error;

  constructor(
    private fb: FormBuilder,
    private programService: ProgramService,
    private courseService: CourseService,
    private tagService: TagService,
    private questionService: QuestionService,
    private router: Router) { }

  ngOnInit() {
    this.programService.listPrograms().subscribe(data => {
      this.programs = data;
    });

    this.programForm = this.fb.group({
      program: [null, Validators.required]
    });

    this.courseForm = this.fb.group({
      course: [null, Validators.required]
    });

    this.tagForm = this.fb.group({
      tag: [null, Validators.required],
      difficulty: [null]
    });

  }

  selectProgram(post) {
    if (post.program !== null) {
      this.courseService.listCoursesByProgram(post.program).subscribe(data => this.courses = data);
    }
  }

  selectCourse(post) {
    if (post.course !== null) {
      this.selectedCourse = post.course;
      this.tagService.listTagsByCourse(post.course).subscribe(data => {
        this.tags = (<any> data).content;
        this.selectedTags = [];
        this.isTagExist = ((this.tags[0].content) ? true : false);
      });

    }
  }

  add(tag) {
    const index1 = this.selectedTags.indexOf(tag);

    if (index1 < 0) {
      this.error = undefined;

      this.selectedTags.push(tag);

      const index2 = this.tags.indexOf(tag);

      if (index2 >= 0) {
        this.tags.splice(index2, 1);
      }
    }
  }

  remove(tag) {
    const index = this.selectedTags.indexOf(tag);

    if (index >= 0) {
      this.selectedTags.splice(index, 1);

      this.tags.push(tag);
    }
  }

  selectTag(stepper: MatStepper) {
    if (this.selectedTags.length === 0) {
      this.error = true;
    } else {
      stepper.next();
    }
  }

  generate() {
    this.questionService.studentQuery(
      this.selectedCourse,
      this.selectedTags.map(t => t.id),
      this.difficulty
    ).subscribe(questions => {
      this.questionService.updateCurrentQuestions(questions);
      this.router.navigate(['student/sheet']);
    });
  }
}
