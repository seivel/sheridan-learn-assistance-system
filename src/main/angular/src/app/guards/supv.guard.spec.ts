import { inject, TestBed } from '@angular/core/testing';

import { SupvGuard } from './supv.guard';

describe('SupvGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupvGuard]
    });
  });

  it('should ...', inject([SupvGuard], (guard: SupvGuard) => {
    expect(guard).toBeTruthy();
  }));
});
