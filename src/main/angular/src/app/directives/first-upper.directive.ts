import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appFirstUpper]'
})
export class FirstUpperDirective {

  constructor(private el: ElementRef) { }

  @HostListener('input', ['$event'])
  onInputChange(event) {
    if (this.el.nativeElement.value.length === 1) {
      if (event.data >= 'a' && event.data <= 'z') {
        event.preventDefault();
        this.el.nativeElement.value = event.data.toUpperCase();
      }
    }
  }

}
