import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appNumOnly]'
})
export class NumOnlyDirective {

  constructor(private el: ElementRef) { }

  @HostListener('input', ['$event'])
  onInputChange(event) {
    const init = this.el.nativeElement.value;

    this.el.nativeElement.value = init.replace(/[^0-9]*/g, '');
    if (init !== this.el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}
