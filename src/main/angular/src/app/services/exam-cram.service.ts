import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Examcram } from '../models/examcram';

@Injectable({
  providedIn: 'root'
})
export class ExamCramService {

  constructor(private http: HttpClient) { }

  listExamCrams() {
    return this.http.get(`${environment.base_path}/examCrams`);
  }

  getPresent() {
    return this.http.get(`${environment.base_path}/examCrams/search/present`);
  }

  listFuture() {
    return this.http.get(`${environment.base_path}/examCrams/search/future`);
  }

  listPast() {
    return this.http.get(`${environment.base_path}/examCrams/search/past`);
  }

  getStats(id: number) {
    return this.http.get(`${environment.base_path}/examCrams/stats/${id}`);
  }

  listStats(from: number, to: number) {
    const body = new HttpParams()
      .set('from', from.toString())
      .set('to', to.toString());

    return this.http.get(`${environment.base_path}/examCrams/stats`, {params: body});
  }

  insertExamCram(examCram: Examcram) {
    const body = {
      'startTime': examCram.startTime,
      'endTime': examCram.endTime,
      'accessCode': examCram.accessCode
    };

    return this.http.post(`${environment.base_path}/examCrams`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  discardExamCram(examCramId: number) {
    return this.http.delete(`${environment.base_path}/examCrams/${examCramId}`);
  }
}
