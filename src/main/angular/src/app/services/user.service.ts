import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  listUsers() {
    return this.http.get(`${environment.base_path}/users`);
  }

  getByUsername(username: string) {
    return this.http.get(`${environment.base_path}/users/search/findByUsername?username=${username}`);
  }

  updateUser(username: string, user: User) {
    const body = new HttpParams()
      .set('firstName', user.firstName)
      .set('lastName', user.lastName);

    return this.http.post(`${environment.base_path}/users/${username}`, body);
  }

  changePassword(username: string, oldPassword: string, newPassword: string) {
    const body = new HttpParams()
      .set('oldPassword', oldPassword)
      .set('newPassword', newPassword);

    return this.http.post(`${environment.base_path}/users/passwordChange/${username}`, body);
  }

  updateUserAvailability(userId: number, availability: boolean) {
    const body = {
      'enabled': availability
    };

    return this.http.patch(`${environment.base_path}/users/${userId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  invite(email: string) {
    const body = new HttpParams()
      .set('email', email);

    return this.http.post(`${environment.base_path}/users/invite`, body);
  }

  registerRequest(ticket: string) {
    const body = new HttpParams()
      .set('ticket', ticket);

    return this.http.post(`${environment.base_path}/users/registerRequest`, body);
  }

  registerCodeRequest(ticket: string) {
    const body = new HttpParams()
      .set('ticket', ticket);

    return this.http.post(`${environment.base_path}/users/registerCodeRequest`, body);
  }

  saveUser(user: User, password: string, ticket: string, vCode: string) {
    const body = new HttpParams()
      .set('username', user.username)
      .set('password', password)
      .set('firstName', user.firstName)
      .set('lastName', user.lastName)
      .set('ticket', ticket)
      .set('vCode', vCode);

    return this.http.post(`${environment.base_path}/users/register`, body);
  }

  passwordResetRequest(email: string) {
    const body = new HttpParams()
      .set('email', email);

    return this.http.post(`${environment.base_path}/users/passwordResetRequest`, body);
  }

  passwordCodeRequest(ticket: string) {
    const body = new HttpParams()
      .set('ticket', ticket);

    return this.http.post(`${environment.base_path}/users/passwordCodeRequest`, body);
  }

  passwordReset(ticket: string, vCode: string, password: string) {
    const body = new HttpParams()
      .set('ticket', ticket)
      .set('vCode', vCode)
      .set('password', password);

    return this.http.post(`${environment.base_path}/users/passwordReset`, body);
  }
}
