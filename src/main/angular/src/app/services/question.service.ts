import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Question } from '../models/question';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private questionSource = new BehaviorSubject('');
  currentQuestions = this.questionSource.asObservable();

  constructor(private http: HttpClient) { }

  updateCurrentQuestions(questions) {
    sessionStorage.removeItem('questions');
    this.questionSource.next(questions);
  }

  /** @deprecated use saveQuestion() instead  **/
  insertQuestion(question: Question, courseIds: number[], tagIds: number[]) {
    this.saveQuestion(question, courseIds, tagIds, null);
  }

  /** @deprecated use updateQuestionAvailability() instead **/
  disableQuestion(questionId: number) {
    this.updateQuestionAvailability(questionId, false);
  }

  /** @deprecated **/
  listCoursesInQuestion(questionId: number) {
    return this.http.get(`${environment.base_path}/questions/${questionId}/courses`);
  }

  /** @deprecated **/
  listTagsInQuestion(questionId: number) {
    return this.http.get(`${environment.base_path}/questions/${questionId}/tags`);
  }

  listQuestions() {
    return this.http.get(`${environment.base_path}/questions`);
  }

  listQuestionById(questionId: number) {
    return this.http.get(`${environment.base_path}/questions/${questionId}?projection=QuestionWithCourseAndTag`);
  }

  saveQuestion(question: Question, courseIds: number[], tagIds: number[], image: File) {
    const body: FormData = new FormData();
    body.append('description', question.description);
    body.append('difficulty', question.difficulty.toString());
    body.append('image', image);

    courseIds.forEach(course => body.append('courses', course.toString()));
    tagIds.forEach(tag => body.append('tags', tag.toString()));

    return this.http.post(`${environment.base_path}/questions`, body);
  }

  updateQuestion(questionId: number, question: Question, courseIds: number[], tagIds: number[], image: File) {
    const body = {
      'description': question.description,
      'difficulty': question.difficulty
    };

    this.http.patch(`${environment.base_path}/questions/${questionId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    }).subscribe(data => {
      console.log('update the question successfully');
    });

    let courses = '';
    for (const cId of courseIds) {
      courses += `/courses/${cId}\n`;
    }
    this.http.put(`${environment.base_path}/questions/${questionId}/courses`, courses, {
      headers: new HttpHeaders().set('Content-Type', 'text/uri-list')
    }).subscribe(data => {
      console.log('update courses successfully');
    });

    let tags = '';
    for (const tId of tagIds) {
      tags += `/tags/${tId}\n`;
    }
    this.http.put(`${environment.base_path}/questions/${questionId}/tags`, tags, {
      headers: new HttpHeaders().set('Content-Type', 'text/uri-list')
    }).subscribe(data => {
      console.log('Update Tag successfully');
    });

    const form: FormData = new FormData();
    form.set('image', image);
    this.http.put(`${environment.base_path}/questions/${questionId}/image`, form).subscribe(data => {
      console.log('Update Image Successfully');
    });

    return 0;
  }

  updateQuestionAvailability(questionId: number, availability: boolean) {
    const body = {
      'available': availability ? 'true' : 'false'
    };

    return this.http.patch(`${environment.base_path}/questions/${questionId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    }).subscribe({error: e => console.error(e)});
  }

  deleteQuestion(questionId: number) {
    return this.http.delete(`${environment.base_path}/questions/${questionId}`);
  }

  studentQuery(courseId: number, tagIds: number[], difficulty: number) {
    let body = new HttpParams()
      .set('difficulty', difficulty.toString())
      .set('courseId', courseId.toString());

    tagIds.forEach(tag => body = body.append('tagIds', tag.toString()));

    return this.http.post(`${environment.base_path}/questions/studentQuery`, body);
  }

  reportQuestion(questionId: number, increase: boolean) {
    const body = new HttpParams()
      .set('id', questionId.toString())
      .set('increase', increase.toString());

    return this.http.post(`${environment.base_path}/questions/report`, body);
  }
}
