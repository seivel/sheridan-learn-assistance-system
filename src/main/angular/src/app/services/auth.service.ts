import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Campus } from '../models/campus.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  EXPIRE_LENGTH = 3600000;

  constructor(
    private http: HttpClient,
    private router: Router) { }

  checkin(code: string, campus: Campus, name: string, response: string) {
    const body = new HttpParams()
      .set('accessCode', code)
      .set('campus', campus.toString())
      .set('name', name)
      .set('recaptchaResponse', response);

    return this.http.post(`${environment.base_path}/students/login`, body);
  }

  login(username: string, password: string, response: string) {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('recaptchaResponse', response);

    return this.http.post(`${environment.base_path}/users/login`, body);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/logout']).catch(console.log);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  isAuthenticated(): boolean {
    const helper = new JwtHelperService();
    const token = this.getToken();

    if (token !== null) {
      return !helper.isTokenExpired(token, 0);
    } else {
      return false;
    }
  }

  setToken(token: string) {
    localStorage.removeItem('token');
    localStorage.setItem('token', token);
  }

  getUsername() {
    const helper = new JwtHelperService();
    const token = this.getToken();

    if (token !== null) {
      return helper.decodeToken(token).sub;
    }

    return null;
  }

  getRoles() {
    const helper = new JwtHelperService();
    const token = this.getToken();

    if (token !== null) {
      return helper.decodeToken(token).auth;
    }

    return null;
  }

  isStudent() {
    return this.getRoles().filter(r => r.authority === 'ROLE_STUDENT').length > 0;
  }

  isAdmin() {
    return this.getRoles().filter(r => r.authority === 'ROLE_ADMIN').length > 0;
  }

  getExpirationDate() {
    const helper = new JwtHelperService();
    const token = this.getToken();

    if (token !== null) {
      return helper.getTokenExpirationDate(token);
    }

    return null;
  }

  isNeedRenew() {
    const expireDate = this.getExpirationDate();

    if (expireDate != null) {
      const now = new Date().getTime();
      return now < expireDate.getTime() &&
        now > (expireDate.getTime() - this.EXPIRE_LENGTH / 2);
    }

    return false;
  }

  isExpired() {
    const expireDate = this.getExpirationDate();

    if (expireDate != null) {
      return new Date().getTime() > expireDate.getTime();
    }

    return true;
  }

  renewToken() {
    const body = new HttpParams()
      .set('token', this.getToken());

    return this.http.post(`${environment.base_path}/users/tokenRenew`, body);
  }
}
