import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Program } from '../models/program';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  constructor(private http: HttpClient) { }

  listPrograms() {
    return this.http.get(`${environment.base_path}/programs`);
  }

  listProgramsFull() {
    return this.http.get(`${environment.base_path}/programs/full`);
  }

  insertProgram(program: Program) {
    const body = {
      'program': program.name
    };

    return this.http.post(`${environment.base_path}/programs`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  updateProgram(programId: number, program: Program) {
    const body = {
      'program': program.name
    };

    return this.http.patch(`${environment.base_path}/programs/${programId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  deleteProgram(programId: number) {
    return this.http.delete(`${environment.base_path}/programs/${programId}`);
  }

}
