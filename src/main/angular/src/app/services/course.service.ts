import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Course } from '../models/course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { }

  listCourses() {
    return this.http.get(`${environment.base_path}/courses`);
  }

  getCourseById(courseId: number) {
    return this.http.get(`${environment.base_path}/courses/${courseId}`);
  }

  listCoursesByProgram(programId: number) {
    return this.http.get(`${environment.base_path}/programs/${programId}/courses`);
  }

  insertCourse(course: Course, programId: number) {
    this.http.post(`${environment.base_path}/courses`,
      {'course': course.name},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')})
      .subscribe(data => {
        const id = (<any> data).id;
        this.http.put(`${environment.base_path}/courses/${id}/program`,
          `/programs/${programId}`,
          {headers: new HttpHeaders().set('Content-Type', 'text/uri-list')}).subscribe();
      });

    return 0;
  }

  updateCourse(courseId: number, course: Course) {
    const body = {
      'course': course.name
    };

    return this.http.patch(`${environment.base_path}/courses/${courseId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  deleteCourse(courseId: number) {
    return this.http.delete(`${environment.base_path}/courses/${courseId}`);
  }
}
