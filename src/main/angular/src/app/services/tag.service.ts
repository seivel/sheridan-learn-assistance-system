import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) { }

  listTags() {
    return this.http.get(`${environment.base_path}/tags`);
  }

  listTagsByCourse(courseId: number) {
    return this.http.get(`${environment.base_path}/courses/${courseId}/tags`);
  }

  insertTag(tag: Tag, courseIds: number[]) {
    this.http.post(`${environment.base_path}/tags`,
      {'tag': tag.tag},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')})
      .subscribe(data => {
        const id = (<any> data).id;
        let body = '';

        for (const cId of courseIds) {
          body += `/courses/${cId}\n`;
        }

        this.http.put(`${environment.base_path}/tags/${id}/courses`, body, {
          headers: new HttpHeaders().set('Content-Type', 'text/uri-list')
        }).subscribe(data => console.log(data));
      });

    return 0;
  }

  updateTag(tagId: number, tag: Tag) {
    const body = {
      'tag': tag.tag
    };

    return this.http.patch(`${environment.base_path}/tags/${tagId}`, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  deleteTag(tagId: number) {
    return this.http.delete(`${environment.base_path}/tags/${tagId}`);
  }
}
