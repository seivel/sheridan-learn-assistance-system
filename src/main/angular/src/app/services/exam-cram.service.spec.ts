import { TestBed } from '@angular/core/testing';

import { ExamCramService } from './exam-cram.service';

describe('ExamCramService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExamCramService = TestBed.get(ExamCramService);
    expect(service).toBeTruthy();
  });
});
