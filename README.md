# Sheridan Exam Cram Builder 

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d117f9ec3c844910be1cc9b7a43a5dc2)](https://www.codacy.com/app/zhangzhu/sheridan-learn-assistance-system?utm_source=seivel@bitbucket.org&amp;utm_medium=referral&amp;utm_content=seivel/sheridan-learn-assistance-system&amp;utm_campaign=Badge_Grade)
[![Build status](https://ci.appveyor.com/api/projects/status/416txci98bb3n400/branch/master?svg=true)](https://ci.appveyor.com/project/seivel/sheridan-learn-assistance-system/branch/master)
[![Website Host](https://img.shields.io/website-up-down-green-red/http/cv.lbesson.qc.to.svg)](https://142.55.32.86:50292/)
[![license:mit](https://img.shields.io/badge/license-mit-blue.svg)](https://opensource.org/licenses/MIT)

Web portal that used by the faculty staff to post questions for students
to review exams.

---

## Development Instruction

### Build Angular (Client)

- `cd src/main/angular`
- `npm install` to install node modules
- `ng serve -o` to start server
- `npm run build` to build
- Build folder is `src/main/resources/static`

### Run Spring Boot (Server)

- Run main method in ExamCramBuilderApplication.java
- Go to `https://localhost:8443`
- Content in the `src/main/resources/static` will be displayed

### Using Maven (Whole Project)

- `mvn spring-boot:run` at root directory

### To Release

- Change server port to 443 in application.properties
- `mvn clean package` to build Maven project
- A .jar is built in `target/`
- Run .jar using `java -jar ***.jar`

---

## Tech Stack

- [Angular](https://angular.io/)
- [Angular Material](https://material.angular.io/)
- [Hibernate](http://hibernate.org/)
- [JSON Web Token](https://jwt.io/)
- [Lombok](https://projectlombok.org/)
- [MySQL](https://www.mysql.com/)
- [Spring Framework](https://spring.io/)

    - [Spring Boot](http://spring.io/projects/spring-boot)
    - [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
    - [Spring Data Rest](https://spring.io/projects/spring-data-rest)
    - [Spring Security](https://spring.io/projects/spring-security)

---

## License

[The MIT Licence](https://bitbucket.org/seivel/sheridan-learn-assistance-system/src/master/LICENSE)
